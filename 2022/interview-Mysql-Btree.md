# [面试篇]Mysql 索引 BTree 与 B+Tree 的区别  ？



## 闲聊

​		说起面试，很多同学都经历过，但是 面试中 可能会遇到各种问题，MySQL 的问题 也是非常多，最近我也经常面试，也希望问一些数据库一些偏理论和底层的东西，来考察同学对技术的理解程度， 之后 我会更新这个系列的 面试。

主要更新的内容主要是: 我经常面试 一些面试者 喜欢问的一些问题,这是 第一篇 就更新 数据库相关的吧





## BTree 基本概念

B树。B树被称为自平衡树，因为它的节点是按顺序遍历排序的。在B树中，一个节点可以有两个以上的孩子。而且高度在每次更新时都会自动调整。在B树中，数据是按照**特定的顺序**排序的，最低值在左边，最高值在右边。在B树中插入数据或键，比二叉树更复杂。



## Btree 的特点

​	

- 节点排序，每个节点 可以存放多个元素，多个元素也是排序的

- 每个节点 key 和数据在一起
- B树的所有叶子节点必须在同一级别
- 在B树的叶子节点上面，不应该有空的子树
- 在关键字全集内做一次查找，性能逼近 二分查找的算法
- 任何关键字出现且只出现在一个节点中
- 搜索有可能在非叶子节点结束，因为数据和索引在一起存储的







## B+Tree 的特点

B+tree 多路平衡查找树

- B+Tree 拥有BTree 的所有的结构特点 

- B+Tree 的非叶子节点不存储数据，只存储关键字，叶子节点才存储了所有的数据，并且是排好序的
- B+Tree 叶子节点是通过指针连接在一起的(双向连接)， 这样在范围查询中发挥作用
- 相对于 Btree , B+tree 层级更低  







来一个 max Degree =3 的一个图

在线生成BTree 的图形

https://www.cs.usfca.edu/~galles/visualization/BTree.html

![image-20220809202248243](image/interview-Mysql-Btree/image-20220809202248243.png)



B+Trees  特点如下

图形生成地址  https://www.cs.usfca.edu/~galles/visualization/BPlusTree.html



![image-20220809203055935](image/interview-Mysql-Btree/image-20220809203055935.png)





## 查找过程的区别

两种索引 查找过程的区别：

B+tree 需要找到叶子节点 才能找到数据， 而Btree 可能不需要找到叶子节点 就可以找到数据





## B+Tree索引 如何提高索引的查询性能 ？ 

1. 找得快， 叶子节点双向指针
2. 一次IO 操作，找更多的数据，减少IO 操作，节点不存数据，只存关键字，这样可以存储更多索引的信息，B+tree 层级会降低





## 为啥 B+Tree 会比 BTree 高度要低呢？ 

页(Page)是Mysql中磁盘和内存交换的基本单位, 也是Mysql管理存储空间的基本单位。

`Page` 是InnoDB存储引擎磁盘管理的最小单位，每个页默认16KB，`innodb_page_size` 可以通过这个参数进行修改

B+Tree 中的非叶子节点 不存储数据， 只存关键字，所以一个Page 中可以容纳更多的索引项，  一是可以降低树的高度，二 在一个内部节点中可以定位更多的叶子节点。









## 参考地址

在线生成BTree 的图形  https://www.cs.usfca.edu/~galles/visualization/BTree.html

图形生成地址  https://www.cs.usfca.edu/~galles/visualization/BPlusTree.html





<center>
    <font color=gray size=1  face="黑体">
 分享快乐,留住感动.  '2022-07-29 22:38:08' --frank
    </font>
</center>