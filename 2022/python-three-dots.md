

# python3中 三个点是啥意思?



## 问题引入

前段时间 看 fastapi 代码的时候， 看类型注解的部分 经常出现 `...` 这样的东西，我就不理解 这是什么意思呢？ 



比如 像下面这个参数 的类型注解，`Callable` 这个类型，我们知道 代表可调用类型， 这里里面有 三个点  是什么意思呢？ 

```python
call: Optional[Callable[..., Any]]
```





来看一下 callalbe 注解如何使用

```python
from typing import Callable


def add(a: int, b: int) -> int:
    pass
    return a + b


def main(fn: Callable[[int, int], int], a: int, b: int):
    return fn(a, b)


if __name__ == '__main__':
    print(main(add, 10, 2))
```



`Callable[[int, int], int]` 这里我定义了一个注解，这个注解的意思，就是这是个可调用的对象，接受两个int 类型的参数，返回 一个int的值。



这里我定义一个 其他的类型， 传入到main 函数中 就会发现 会给出一个警告。

![image-20220807200222008](image/python-three-dots/image-20220807200222008.png)



Callable  这个对象如果接受两个值，第一个值 为一个list，list存放参数的类型， 第二值存放 可调用对象的返回值returnType 

`Calllabe[[a,b,c...],ReturnType]` 类似于这种结构， 返回类型只能是单一类型。





## 第一种用法

`Callable[..., ReturnType]` （省略号字面量）可用于为接受**任意数量参**数，并返回 `ReturnType` 的可调对象提供类型提示。

如果是这样子的形式，表示 参数类型，以及数量不做限制，返回类型为 Returntype 类型。 

所以说 纯 [`Callable`](https://docs.python.org/zh-cn/3/library/typing.html#typing.Callable) 等价于 `Callable[..., Any]` 





看下面的代码， 也就明白了， 这里`endpoint` 就是 一个可调用对象 即可，返回类型也是任意的。

```python
def add_api_websocket_route(
  self, path: str, endpoint: Callable[..., Any], name: Optional[str] = None
) -> None:
  	pass
```









## 第二种用法 三个点的含义



有的时候 ellipsis 省略号 出现在 函数的定义的后面，此时这个时候就是 占位符的意思， 相当于 `pass` 的感觉， 本身并没有什么 实际含义，只是表明这是一个代码块。 如果没有`pass` 或者 `...` 会报语法的错误。



看下面的例子 就是这样子

```python
def caculate(age: int) -> int: ...
```



```python
def caculate(age: int) -> int:
    ...

```





## 第三种用法 必选项



在[pydantic](https://github.com/pydantic/pydantic) 这个库中，对于字段验证的时候，也可以传入 `...` 来表示 这个字段是必选的。 

来看下一个例子

```python
from pydantic import BaseModel, Field


class Item(BaseModel):
    # ... 这里表示必须的意思
    name: str = Field(...)
    price: float = Field(default=10)

      
if __name__ == '__main__':
    item = Item(name='book', price=13.8)
    item2 = Item(price=13.8)
    print(item2)

```

item2 会报错，因为没有传入name 的值，这个值 不能为空，为必选项。

报错如下：

```bash
Traceback (most recent call last):
  File "/Users/frank/code/py_proj/study-fastapi/helloworld/mymodel.py", line 21, in <module>
    item2 = Item(price=13.8)
  File "pydantic/main.py", line 341, in pydantic.main.BaseModel.__init__
pydantic.error_wrappers.ValidationError: 1 validation error for Item
name
  field required (type=value_error.missing)
```



顺便提一下，如果想让这个字段是可选的，添加 `Optional` 这个类型就可以了.

```python
from typing import Optional

from pydantic import BaseModel, Field


class Item(BaseModel):
    # 可选的字段值，字段值默认为None 
    name: Optional[str] = Field(default=None)
    price: float = Field(default=10)


if __name__ == '__main__':
    item2 = Item(price=13.8)
    print(item2) # name=None price=13.8
```









## 总结

文本总结了我看代码中遇到的困惑，省略号的用法， 当然可能还有其他的用法，欢迎补充，或者评论。







## 参考文档



[Callable docs](https://docs.python.org/3/library/typing.html#typing.Callable)

[stackoverflow question](https://stackoverflow.com/questions/70603143/what-is-the-meaning-of-three-dots-in-python-code-and-where-can-i-see-the-f)

[pydantic github](https://github.com/pydantic/pydantic)







<center>  
    <font color=gray size=1  face="黑体">
 分享快乐,留住感动.  '2022-08-07 20:41:01' --frank 
    </font>
</center>














