# python3.8 新的特性介绍



[TOC]



python3.8 已经发布有一段时间，最近一段时间 才看这个版本的特性， 有一些优化，

增加了一下 东西，觉得挺好的。

1. 海象运算符  `:=`
2. fstring 增强版
3. def 函数 指定 位置，关键字参数 
4. 模块细节调整
4. 新增装饰器 cache_property





## 海象运算符

这个都用这个翻译 `:=`  这个符号，可能这个符号看起来像海象吧。 这个作用可以把等号右面表达式的值 赋值给 这个符号 前面的变量。

举个例子 就明白了。

列表长度判断

比如 我希望 判断一个 list 长度 如果小于等于5 ,我就打印一下，如果大于5  我只输出前5 个元素

```python
nums = list(range(10))

length = len(nums)
if length <= 5:
    print(nums)
else:
    print(nums[0:5])
```



这样 你要定义变量 length ，并且这个变量可能 之后 就不会再用了， 这样当然没有什么 问题，其实不够简洁。

```python
nums = list(range(10))

# length = len(nums)
if (length:=len(nums)) <= 5:
    print(nums)
else:
    print(nums[0:5])
```

这样看起来 是不是比较优雅一点啊。





读取文件，判断文件 有没有读完，每次读取一行， 最后 看有没有读完 

```python
# 伪代码 

def process(block):
    print(f"{block=}")


with open('names.txt', 'r') as f:
    while (block := f.readline()) != '':
        process(block)
```





## fstring 增强

在python3.7 里面 已经可以支持fstring 这种写法，`{name}` 这样就会解析这个变量了。



```python
name = 'frank'
hobby = 'swiming'
print(f"name={name},hobby={hobby}")
print(f"name={name!r},hobby={hobby!r}")

"""结果如下
name=frank,hobby=swiming
name='frank',hobby='swiming'
"""
```

上面的print 发现有一个 `name=` 和里面的大括号里面的name 是重复的。  在python3.8 中 ,在 大括号里面 加一个 `=` 就可以了，是不是很完美，这种打印 是原生字符串会把引号 打印出来， 当然 可以指定格式打印了 。python 3.7  方法 一样可以指定格式，格式写在`=`号的后面。

```python
name = 'frank'
hobby = 'swiming'
print(f"{name=},{hobby=}")

"""结果如下
name='frank',hobby='swiming'

"""
```

像下面 可以指定具体的格式， 是不是很好啊。

```python
print(f"name={name!s},hobby={hobby!s}")
print(f"name={name!r},hobby={hobby!r}")
```







## 函数增强 

这个功能 其实一直都有，只是在这之前没有正式做feature， 就是 可以指定一些参数只能传 位置参数，一些参数 只能传入关键字参数， 这样 有一定好处，强行 让调用者 安装 规则调用 函数，这样比较清晰一点。

 




以下面的`fun函数` 为例 ， 有一个`/` 这里之前的变量 只能传入**位置参数**， `*` 之后的变量只能用关键字传入进来，

在 `/` 和 `*` 之间的变量 ，可以选择 用 位置 或者关键字传入参数 ， 这里不要求 全部都是使用这个函数声明，有的话，就按照这种约定来。





```python
def fun(a, b, /, c, d, *, e, f):
    print(f"{a=},{b=},{c=},{d=},{e=},{f=}")

    

fun(1, 2, 3, 4, e=5, f=6) # ok 
fun(1, 2, c=3, d=4, e=5, f=6) #ok 
fun(1, 2, 3, d=4, e=5, f=6) # ok 


fun(1, 2, c=3, 4, e=5, f=6) # error 1 
fun(a=1,b=2,3,4,e=5,f=6) # error 2
fun(1,2,3,4,5,f=6)  # error 3
fun(1,b=2,3,4,e=5,f=6) # error 4

```



第一种错误， 因为 c 传入关键字参数 ，d 是位置参数 。因为 python 中位置参数 是在 关键字参数后面的。

```python
Python 3.8.2 (tags/v3.8.2:7b3ab59, Feb 25 2020, 23:03:10) [MSC v.1916 64 bit (AMD64)] on win32
>>> 
... def fun(a, b, /, c, d, *, e, f):
...     print(f"{a=},{b=},{c=},{d=},{e=},{f=}")
...     
>>> fun(1, 2, c=3, 4, e=5, f=6)
  File "<input>", line 1
SyntaxError: positional argument follows keyword argument
```



第二种错误， a,b  只能是位置参数， 传入关键字 肯定不对啊。

```python
>>> fun(a=1,b=2,3,4,e=5,f=6)
  File "<input>", line 1
SyntaxError: positional argument follows keyword argument
```



第三种错误，e,f   只能是关键字 参数， e 传入的是位置参数。

```python
>>> fun(1,2,3,4,5,f=6)
Traceback (most recent call last):
  File "<input>", line 1, in <module>
TypeError: fun() takes 4 positional arguments but 5 positional arguments (and 1 keyword-only argument) were given

```



第四种错误， a,b  只能是位置参数， 传入关键字 肯定不对啊。

```python
>>> fun(1,b=2,3,4,e=5,f=6)
  File "<input>", line 1
SyntaxError: positional argument follows keyword argument
```





## 新的模块 metadata

这个模块  `importlib.metadata`  可以查看 第三方package 一些元信息，依赖等 

```pyt
from importlib.metadata import version, requires, files
```



首先 本机要安装 flask 才能看，不能直接 去pypi 看版本，这个只是看你本地按照的环境的信息

```python
>>> from importlib.metadata import  requires,version,files
... 
>>> version('flask')
'1.1.1'
>>> version('Flask')
'1.1.1'
>>> requires('flask')
>>> list(files('flask'))[:2]
[PackagePath('../../Scripts/flask.exe'), PackagePath('Flask-1.1.1.dist-info/INSTALLER')]
```







## functools.lru_cache 调用方式的改进

@functools.lru_cache() 这里要有一个括号，现在 两种方式都可以兼容 了。

```python
import functools

@functools.lru_cache()
def fun():
    pass


@functools.lru_cache
def fun2():
    pass


```



## 新增装饰器 cache_property

我们知道在python3中有一个装饰器 `property`   可以把一个方法 转换为 一个属性，同时可以对属性进行控制。如果你不太熟悉  `property`   建议看看这篇文章 [python3中的特性property介绍](https://blog.csdn.net/u010339879/article/details/102233924)



property 有一个问题会存在 属性重复计算的问题， 能不能有办法把 `property` 结果 缓存下来呢？ 它来了，就是 3.8 新增了一个装饰器

`cache_property`  ，来看一个例子



有一个学生类，有三门学科成绩，作为属性，希望计算学生的平均成绩，和总成绩。

```python
class Student:

    def __init__(self, math=0, chinese=0, english=0):
        self.math = math
        self.chinese = chinese
        self.english = english
        pass

    @property
    def total(self):
        print("total begin ...")
        return self.math + self.chinese + self.english

    @property
    def avg(self):
        print("avg begin ...")
        return round(self.total / 3, 2)
        pass

if __name__ == '__main__':
    s = Student(english=80, math=60, chinese=90)

    print(s.total)
    print(s.total)
    print(s.avg)
```

运行结果如下：

```reStructuredText
total begin ...
230
total begin ...
230
avg begin ...
total begin ...
76.67
```



我们发现每次调用 total , avg 都会重新 计算这个total 方法， 这样就会出现重复计算的问题，造成了大量的重复计算。



我们可以使用 `cached_property` 来缓存结果 来提高效率

```python
from functools import cached_property


class Student:

    def __init__(self, math=0, chinese=0, english=0):
        self.math = math
        self.chinese = chinese
        self.english = english
        pass

    @cached_property
    def total(self):
        print("total begin ...")
        return self.math + self.chinese + self.english

    @cached_property
    def avg(self):
        print("avg begin ...")
        return round(self.total / 3, 2)
        pass


if __name__ == '__main__':
    s = Student(english=80, math=60, chinese=90)

    print(s.total)
    print(s.total)
    print(s.avg)
    print(s.avg)

```



运行 结果如下：

```reStructuredText
total begin ...
230
230
avg begin ...
76.67
76.67
```



发现 `total` , `avg` 只运行了一次，后面再次运行，都是从缓存获取的值，而不是每次重新计算属性，这个就是 `cached_property` 的用法







## 总结

当然还有一些 模块细节调整，asynicio 模块的稳定版， 一些细节优化，具体 看下 下面的参考链接，这里之列了一些 我感觉比较常用的一些改变。 





## 参考文档 

[pep-0572](https://www.python.org/dev/peps/pep-0572/)

[new-feature]( https://docs.python.org/3/whatsnew/3.8.html)

[pyton3.8 functools](https://docs.python.org/zh-tw/3.8/library/functools.html)

[python3中的特性property介绍](https://blog.csdn.net/u010339879/article/details/102233924)





<center>  <font color=gray size=1  face="黑体">
   分享快乐,留住感动. 2020-03-27 18:50:19   --frank 
	   </font>
</center>