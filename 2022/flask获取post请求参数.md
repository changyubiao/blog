# flask获取post请求参数

[TOC]

今天 我们继续来说一下， 使用flask web application, 如何获取post 请求参数的。

## 概述

对于post请求 有几种方式。 **`Content-Type`** 实体头部用于指示资源的MIME类型 [media type](https://developer.mozilla.org/zh-CN/docs/Glossary/MIME_type) 。

content-type是http请求头的字段。作为请求头时（post或者put），客户端告诉服务器实际发送的数据类型。

对于不同的content-type 发送的数据不太一样，对于服务器端，需要如何获取数据，以及正确解析的方法也是不一样的。

下面列出常用的几种 **`Content-Type`** 

- application/json

- application/x-www-form-urlencoded

- multipart/form-data

- text/plain 

- text/xml

- text/html 




## 1. `application/json`

Content-Type 以这种方式的api ,目前来说还是比较多的，现在主流前后端交互的api,使用`application/json` 这种方式 传输数据 非常常见。 主要得意于json 的发展，以及前后端完善的库支持，使得这种方式 比较常见。



这里以login 登录接口为例

### http 请求报文格式如下：

```reStructuredText
POST /login HTTP/1.1
Host: 127.0.0.1:5000
Content-Type: application/json
Content-Length: 59

{
    "username":"frank",
    "password":"jsdofjdsofjsdo"
}

```



### 使用curl 

```bash
curl --location --request POST 'http://127.0.0.1:5000/login' \
--header 'Content-Type: application/json' \
--data-raw '{
    "username":"frank",
    "password":"jsdofjdsofjsdo"
}'
```





### postman 请求

![image-20220108230554204](image/flask获取post请求参数/image-20220108230554204.png)





### flask如何获取请求体呢

对于这种请求方式 ，flask 如何获取请求的body 的内容呢？ 

```python
# main.py
from flask import Flask
from flask import request
from flask import jsonify
# pip install pysimple-log
from simplelog import logger

app = Flask(__name__)


@app.route('/login', methods=[ 'POST'])
def login():
    if request.method == 'POST':
        # 这样获取就可以了 
        json_data = request.json
        logger.info(f"json_data:{json_data}")
        return jsonify(json_data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

```



我们直接可以通过， flask 提供的request对象获取参数。 `request.json` 通过这个属性 就可以获取到 request body 的内容了。是不是很方便呢。







 ## 2. `application/x-www-form-urlencoded`

这种方式是 浏览器原生form表单默认的提交方式

http 请求报文格式

```reStructuredText
POST /login HTTP/1.1
Host: 127.0.0.1:5000
Content-Type: application/x-www-form-urlencoded
Content-Length: 38

username=frank&password=jsdofjdsofjsdo
```





### 使用curl 请求

```bash
curl --location --request POST 'http://127.0.0.1:5000/login' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=frank' \
--data-urlencode 'password=jsdofjdsofjsdo'
```



### flask如何获取请求体呢

对于这中content-type 我们可以使用 `request.form` 返回一个不可变的字典类型。 然后就可以获取 request body 啦。

```python
# 省略 ...
@app.route('/login', methods=[ 'POST'])
def login():
    if request.method == 'POST':
        json_data = request.form
        logger.info(f"json_data:{json_data}")
        return jsonify(json_data)
```











 ##  3. `multipart/form-data`

这又是一个常见的 POST 数据提交的方式。我们使用表单**上传文件**时，必须让 **form** 表单的enctype 等于 multipart/form-data。直接来看一个请求示例：

这里我只是演示 一下，对应 http报文

http 请求报文

```reStructuredText
POST /login HTTP/1.1
Host: 127.0.0.1:5000
Content-Type: multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Length: 239

----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="username"

frank
----WebKitFormBoundary7MA4YWxkTrZu0gW
Content-Disposition: form-data; name="password"

111111sfsfsafsafas==
----WebKitFormBoundary7MA4YWxkTrZu0gW
```





### 使用curl 请求

```bash
curl --location --request POST 'http://127.0.0.1:5000/login' \
--form 'username="frank"' \
--form 'password="111111sfsfsafsafas=="'
```





### postman 请求

![image-20220108233313255](image/flask获取post请求参数/image-20220108233313255.png)





### flask如何获取请求体呢

对于这种方式还是可以通过 `request.from` 来获取参数 

```python
# ... 省略
@app.route('/login', methods=[ 'POST'])
def login():
    if request.method == 'POST':
        json_data = request.form
        logger.info(f"json_data:{json_data}")
        return jsonify(json_data)

```





## 4. text/plain 



用纯文本发送数据

### http 请求报文如下

```bash
POST /login HTTP/1.1
Host: 127.0.0.1:5000
Content-Type: text/plain
Content-Length: 59

{
    "username":"frank",
    "password":"jsdofjdsofjsdo"
}
```



### 使用curl 请求

```bash
curl --location --request POST 'http://127.0.0.1:5000/login' \
--header 'Content-Type: text/plain' \
--data-raw '{
    "username":"frank",
    "password":"jsdofjdsofjsdo"
}'
```





### postman 请求

![image-20220109111502250](image/flask获取post请求参数/image-20220109111502250.png)







### flask如何获取请求体呢

flask 如何正常接收数据呢？ 原生的请求数据会放在 `request.data` 里面，类型是bytes，需要转成字符串，然后通过json 模块转成字典。

```python
import json
from flask import Flask
from flask import request
from flask import jsonify
# pip install pysimple-log
from simplelog import logger


app = Flask(__name__)



@app.route('/login', methods=['POST'])
def login():
    if request.method == 'POST':
        # bytes 类型
        raw_data = request.data
        logger.info(f'raw_data:{raw_data}')
        json_data = json.loads(raw_data.decode())
        logger.info(f"json_data:{json_data}，type:{type(json_data)}")
        return jsonify(json_data)
```



结果如下： 

![image-20220109111248489](image/flask获取post请求参数/image-20220109111248489.png)

可以看出可以正常解析数据了，并且以json 的形式返回了。



### 总结

本文主要说明了使用flask 框架，如果获取常见的post请求body 数据，今天的分享就到这里了。





### 参考文档

[accessing-request-data](https://flask.palletsprojects.com/en/2.0.x/quickstart/#accessing-request-data)

[POST 方法的content-type类型](https://zhuanlan.zhihu.com/p/129057481) 

[content-type mdn]( https://developer.mozilla.org/zh-CN/docs/Web/HTTP/Headers/Content-Type)  







<center>
		<font color=gray size=1  face="黑体">
   分享快乐,留住感动.  '2022-01-09 12:06:45' --frank 
	  </font>
</center>

