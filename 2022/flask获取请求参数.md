# Flask 获取GET参数





Flask算是比较容易上手的一个web application 框架，这篇文章就简单探讨一下，使用flask 来获取GET请求参数。



### flask 最小demo 

下面编写 main.py 一个最简单的服务，实现search接口.

```python
from flask import Flask
from flask import request
from flask import jsonify


app = Flask(__name__)


@app.route('/search', methods=['GET', 'POST'])
def index():
    gender = request.args.get('gender')
    name = request.args.get('name')

    return jsonify({'name': name, 'gender': gender})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)

```



我们运行这个脚本

```py
python  main.py 
```





在浏览器输入URL   http://localhost:5000/search?name=frank&gender=male



这里请求参数 就是? 后面的键值对 有两个.一个是 name ,一个是gender



![image-20220106214930013](image/flask获取请求参数/image-20220106214930013.png)

可以看到成功获取到了querystring 参数。





### 获取GET请求参数

我们可以通过 request.args 这个不可变字典 ，根据key 来获取 GET请求的请求参数

```python 
from flask import request

value= request.args.get('key', '')
```





### 参考文档

[accessing-request-data](https://flask.palletsprojects.com/en/2.0.x/quickstart/#accessing-request-data)




<center>  <font color=gray size=1  face="黑体">
   分享快乐,留住感动.  '2022-01-06 21:56:13' --frank 
	   </font>
</center>
