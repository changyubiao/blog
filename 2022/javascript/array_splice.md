# Javascript数组的 splice 方法介绍



**`splice()`** 方法通过删除或替换现有元素或者原地添加新的元素来修改数组,并以数组形式返回被修改的内容。此方法会改变原数组。

语法格式

```
array.splice(start[, deleteCount[, item1[, item2[, ...]]]])
```

### [参数](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/splice#参数)

- `start` 必选

  指定修改的开始位置（从0计数）。如果超出了数组的长度，则从数组末尾开始添加内容；如果是负值，则表示从数组末位开始的第几位（从-1计数，这意味着-n是倒数第n个元素并且等价于`array.length-n`）；如果负数的绝对值大于数组的长度，则表示开始位置为第0位。

- `deleteCount` 可选

  整数，表示要移除的数组元素的个数。如果 `deleteCount` 大于 `start` 之后的元素的总数，则从 `start` 后面的元素都将被删除（含第 `start` 位）。如果 `deleteCount` 被省略了，或者它的值大于等于`array.length - start`(也就是说，如果它大于或者等于`start`之后的所有元素的数量)，那么`start`之后数组的所有元素都会被删除。如果 `deleteCount` 是 0 或者负数，则不移除元素。这种情况下，至少应添加一个新元素。

- `item1, item2, *...*` 可选

  要添加进数组的元素,从`start` 位置开始。如果不指定，则 `splice()` 将只删除数组元素。





### [返回值](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/splice#返回值)

由被删除的元素组成的一个数组。如果只删除了一个元素，则返回只包含一个元素的数组。如果没有删除元素，则返回空数组。



### [描述](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/splice#描述)

如果添加进数组的元素个数不等于被删除的元素个数，数组的长度会发生相应的改变。





## 牛刀小试



现在有一个场景，我和我的室友们，现在在一个数组里面。 按照排行分别为:  老大，老二，老三, .. ,老六, 小七.

这里 我用 下面的数组进行举例

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan','Frank','Guan laowu','Yang laoliu','Li xiaoqi'];
```



### 删除元素

删除一个元素,删除 某个位置的元素

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan','Frank'];

// start 开始位置，从零开始 ， deleteCount 删除个数,从1 开始
// arr.splice(start,count)
// 删除第0位置的一个元素
arr.splice(0,1);
console.log('arr=',arr);
```



![image-20220328150543205](image/array_splice/image-20220328150543205.png)

可以看到 第0号位置 `'Liu laoda'`  被删除了。



删除前面两个元素 

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan','Frank'];
// 删除 前两个元素
arr.splice(0,2);
console.log('arr=',arr);  // ['Wei laosan', 'Frank']
```





### 添加元素 并且替换元素



splice 删除元素后 会返回被删除的元素，返回类型是一个 Array 类型.



#### example1

这里可以进行元素替换，替换之后直接会影响 原来的数组的。返回的是删除的数组元素

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan','Frank'];

// 添加元素,删除元素 用 Guan laowu 替换掉
// 从1号位置删除一个元素，插入 元素 'Guan laowu'
// 返回被删除的元素
console.log('before arr=',arr);
var removed = arr.splice(1, 1, 'Guan laowu');
console.log('arr=',arr);
console.log('removed=',removed);   // removed.constructor.name  == Array
```



![image-20220328151734701](image/array_splice/image-20220328151734701.png)



#### example2

删除的个数和添加的元素的个数可以是不一样的。 

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan','Frank'];
console.log('before arr=',arr);
// 删除 idx=1的元素,并且 添加 在此位置 依次添加4个元素。
var removed = arr.splice(1, 1, 'Guan laowu','A','B','C');
console.log('arr=',arr);
console.log('removed=',removed);   // removed.constructor.name  == Array
```



![image-20220328154855890](image/array_splice/image-20220328154855890.png)



#### example3



删除元素的例子，如果没有在第三个参数上有任何元素，则表示删除元素了。

start 开始位置 ，deleteCount 删除个数，如果删除的个数大于数组本身长度，则全部删除了。

```javascript
var arr = ['Liu laoda', 'Li laoer', 'Wei laosan', 'Frank','Guan Laowu','Yang Laoliu','Li xiaoqi'];
console.log('before arr=', arr);
// 从数组 下标为2的位置, 删除3个元素
var removed = arr.splice(2, 3);  // ['Wei laosan', 'Frank', 'Guan Laowu']
console.log('arr=', arr); //['Liu laoda', 'Li laoer', 'Yang Laoliu', 'Li xiaoqi']
console.log('removed=', removed);// ['Wei laosan', 'Frank', 'Guan Laowu']
```



注意这里删除元素 直接对原来的数组修改， 返回值 就是删除的元素的数组。

同时数组的length 属性，维护成正确的值。

![image-20220328152841787](image/array_splice/image-20220328152841787.png)



#### example4 

如果删除的元素个数，超过了后面到数组的最大长度，则后面的元素全部被删除了。

比如 从下标2 后面有 5个元素， 此时 deleteCount = 10 ,那么 后面的元素下标>=2 ，全部删除掉

```javascript
			var arr = ['Liu laoda', 'Li laoer', 'Wei laosan', 'Frank','Guan Laowu','Yang Laoliu','Li xiaoqi'];
			
			console.log('before arr=', arr);
			// 从数组 下标为2的位置, 删除10个元素
			var removed = arr.splice(2, 10);  // ['Wei laosan', 'Frank', 'Guan Laowu']
			console.log('arr=', arr); // ['Liu laoda', 'Li laoer']
			console.log('removed=', removed);// ['Wei laosan', 'Frank', 'Guan Laowu', 'Yang Laoliu', 'Li xiaoqi']
```







### 添加元素



#### example1

在idx=2 的位置 添加 'Frank' 元素， 

此时 只要把 deleteCount = 0,表示 删除0个元素 ,表示在此位置上添加元素。

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan',];
console.log('before arr=',arr);
// 在 idex=2 的位置 添加 'Frank' 元素
var removed = arr.splice(2, 0,'Frank');
console.log('arr=',arr);
console.log('removed=',removed);
```



![image-20220328155422631](image/array_splice/image-20220328155422631.png)



#### example2

#### 在数组的头部添加元素,  Frank

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan',];
console.log('before arr=',arr);
// 在 idex=0 的位置 添加 'Frank' 元素
var removed = arr.splice(0, 0,'Frank');
console.log('arr=',arr);
console.log('removed=',removed);  // []
```



#### 在数组尾部添加元素, Frank

```javascript
var arr = ['Liu laoda','Li laoer','Wei laosan'];
console.log('before arr=',arr);
// 在数组 尾部 添加 Frank
var removed = arr.splice(arr.length, 0,'Frank');
console.log('arr=',arr);  
console.log('removed=',removed);  // []
```





### 负数索引支持



删除后面两个元素

```javascript
// 负数索引
var arr = ['Liu laoda', 'Li laoer', 'Wei laosan', 'Frank','Guan Laowu'];
console.log('before arr=', arr);
// 删除后两个元素
var removed = arr.splice(-2);  
console.log('arr=', arr); // ['Liu laoda', 'Li laoer', 'Wei laosan']
console.log('removed=', removed); // ['Frank', 'Guan Laowu']
```



删除数组最后一个元素 

```javascript
var arr = ['Liu laoda', 'Li laoer', 'Wei laosan', 'Frank','Guan Laowu'];
console.log('before arr=', arr);
// 删除后1个元素
var removed = arr.splice(-1);  
console.log('arr=', arr);  // ['Liu laoda', 'Li laoer', 'Wei laosan', 'Frank']
console.log('removed=', removed);  // ['Guan Laowu']

```



## 总结一下

`array.splice(start[, deleteCount[, item1[, item2[, ...]]]])`  这个函数 可以实现 对数组的增加，删除 ，替换。

start 就是开始的下标位置，必选参数. 

deleteCount  删除多少个元素 ， 如果没有指定那么 一直会删除到数组的最后位置。

最后 一个参数，表示 删除后要填充其他的元素，如果要就写入填入的元素即可。如果不需要，就是单纯的删除元素。





参考链接 

[MDN array splice ]( https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/splice)





<center>  <font color=gray size=1  face="黑体">
   分享快乐,留住感动.  '2022-04-05 19:35:10' --frank 
	   </font>
</center>







