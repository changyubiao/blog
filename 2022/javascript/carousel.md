# JS实现轮播图



轮播图原理 下面 先介绍一下 轮播的组成部分



container 区域被称为 可视区， 然后超出个 区域的 内容 设置隐藏

![image-20220707230627424](image/carousel/image-20220707230627424.png)







在container 区域下面 有一个 carousel 区域 这个区域 放了 将要移动的图片. 我这里使用 flex 布局 把图片放在 carousel 区域

![image-20220707231117074](image/carousel/image-20220707231117074.png)





下面样式html以及 css 的样式文件

```HTML
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>流浪法师</title>

		<script src="./slides.js" defer></script>
		<style>
			* {
				padding: 0;
				margin: 0;
				box-sizing: border-box;
			}

			h1 {
				text-align: center;
				margin: 25px auto;
			}

			.container {
				position: relative;
				margin: 30px auto;
				width: 782px;
				height: 573px;
				/* overflow: hidden; */
				background-color: #f5f5f5;
			}


			.carousel {
				display: flex;
				flex-wrap: nowrap;
				transition: all 0.2s;
			}

			.carousel>.item {
				width: 782px;
				height: 573px;
				flex: 0 0 782px;
			}

			.carousel>.item>img {
				height: 100%;
				width: 100%;
				object-fit: cover;
				object-position: center top;

			}

			.container .left,
			.container .right {
				width: 70px;
				height: 70px;
				position: absolute;
				top: 50%;
				transform: translateY(-50%);
				text-align: center;
				color: #f1f1f1;
				line-height: 55px;
				font-size: 55px;
				vertical-align: bottom;
				background: rgba(28, 31, 33, .1);
				border-radius: 50%;
			}


			.container .left {
				left: 0;
			}

			.container .right {
				right: 0;
			}

			.container .left:hover,
			.container .right:hover {
				background: rgba(28, 31, 33, .5);
				border-radius: 50%;
				cursor: pointer;
				font-weight: 500;
				color: #fff;
			}
		</style>
	</head>
	<body>

		<div class="wrapper">
			<h1>流浪法师</h1>
			<div class="container">
				<div class="carousel">
					<div class="item"><img src="./img/守护者雕像 瑞兹1.jpeg" alt=""></div>
					<div class="item"><img src="./img/美国大叔 瑞兹2.jpeg" alt=""></div>
					<div class="item"><img src="./img/部落精神 瑞兹3.jpeg" alt=""></div>
					<div class="item"><img src="./img/符文法师4.jpeg" alt=""></div>
				</div>

				<div class="left" onclick="">&lt;</div>
				<div class="right" onclick="">&gt;</div>

			</div>
		</div>

	</body>
</html>

```





## 思考 轮播图是如何移动的？ 



 当点击右移的时候， 我们只需要把coursel 区域 整体向左移动 一个图像宽度的距离即可。

![image-20220707232359938](image/carousel/image-20220707232359938.png)



移动后的情况如下：

![image-20220707232738985](image/carousel/image-20220707232738985.png)







## 代码实现

我们需要在点击 右移按钮的时候， 让 `carousel` 区域 X 方向左移即可。 

我们需要在 `left`, `right`  绑定click 事件 即可

javascript 原始 绑定 可以使用下面的方法， `DOM.addEventListener()` 这种方式来绑定事件.

```javascript
left.addEventListener('click', function(){});
left.addEventListener('click', function(){});
```





代码的具体实现， 实现这个`function`

获取对应的dom 添加 style 样式 即可 ， 有一个样式是 `transform: translateX() ;`  向`X方向`移动。

边界的控制

 我们 边界问题比如移动到最后一张图片，该如何操作，以及在第一张图片的时候， 点击左移如何处理。

我们先来一个最简单的版本，就是  `carousel`  移动到边界的时候不做任何处理，不移动，不操作。

### 版本1

下面 是 `slide.js`

```javascript
// 获取dom 结点 
let left = document.querySelector('.left');
let right = document.querySelector('.right');
let carousel = document.querySelector('.carousel');

// 图片总的数量
const imgCount = 4;

// 一张图片基本宽度
const imgWidth = 782;

// 默认第一张图片偏移 的位置
let currentX = 0;

// 边界位置 
const maxTranslateX = imgWidth * (imgCount - 1);

left.addEventListener('click', function() {
	// console.log('click left');
	if (currentX < 0) {
		// 没有超出范围 正常移动即可
		currentX += imgWidth;
	}
	// setting  translateX  偏移方向
	carousel.style.transform = `translateX(${currentX}px)`;
})


right.addEventListener('click', function() {
	// console.log('click right');
	// 没有超出范围就正常移动，currentX  - imgWidth
	if (Math.abs(currentX) < maxTranslateX) {
		currentX -= imgWidth;
	}
	// setting  translateX  偏移方向
	carousel.style.transform = `translateX(${currentX}px)`;
})

```





### 版本2

第二个版本

图片循环 移动，如果图片到了最后一张，则显示第一张图片 ， 如果第一张图片左移，直接到最后 一张图片 即可。



下面是 `slide2.js`

```javascript	
// 获取dom 结点 
let left = document.querySelector('.left');
let right = document.querySelector('.right');
let carousel = document.querySelector('.carousel');

// 图片总的数量
const imgCount = 4;

// 一张图片基本宽度
const imgWidth = 782;

// 默认第一张图片偏移 的位置
let currentX = 0;

// 边界位置
const maxTranslateX = imgWidth * (imgCount - 1);


left.addEventListener('click', function() {
	// console.log('click left');
	if (currentX === 0) {
		// 显示最后一张图片
		currentX += imgWidth * (imgCount - 1) * -1;
	} else if (Math.abs(currentX) <= maxTranslateX) {
		// 没有超出范围 正常移动即可
		currentX += imgWidth;
	} else {
		currentX = 0;
	}
	// setting  translateX  偏移方向
	carousel.style.transform = `translateX(${currentX}px)`;
})



right.addEventListener('click', function() {
	// console.log('click right');
	// 没有超出范围就正常移动，currentX  - imgWidth
	if (Math.abs(currentX) < maxTranslateX) {
		currentX -= imgWidth;
	} else {
		// 显示第一张 幻灯片
		currentX = 0;
	}
	// setting  translateX  偏移方向
	carousel.style.transform = `translateX(${currentX}px)`;
})

```





## 总结

本文使用了 css3 的属性，`transition` , `transform`  来实现图片的轮播效果，相对简单的一点实现。

# 

[mdn  css transform](https://developer.mozilla.org/zh-CN/docs/Web/CSS/transform)

[mdn css transition](https://developer.mozilla.org/zh-CN/docs/Web/CSS/CSS_Transitions/Using_CSS_transitions)







<center>  
    <font color=gray size=1  face="黑体">
 分享快乐,留住感动.  '2022-07-08 17:47:05' --frank 
    </font>
</center>
