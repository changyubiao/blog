# Javascript数组的 forEach 方法介绍



在JavaScript 中数组的遍历 有很多中方法， 其中有一种 使用 foreach 来遍历数组。

mdn官方文档如下：

## [语法](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach#语法)

```
arr.forEach(callback(currentValue [, index [, array]])[, thisArg])
```



### [参数](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach#参数)

- `callback`

  为数组中每个元素执行的函数，该函数接收一至三个参数：`currentValue`数组中正在处理的当前元素。`index` 可选数组中正在处理的当前元素的索引。`array` 可选`forEach()` 方法正在操作的数组。

- `thisArg` 可选

  可选参数。当执行回调函数 `callback` 时，用作 `this` 的值。



在forEach 中传入一个 callback 函数， 函数最多可以接收 三个值， 分别为当前正在遍历的值， 当前值对应的索引，以及当前数组本身



### 牛刀小试 



现在有一个场景，我和我的室友们，现在在一个数组里面。 按照排行分别为:  老大，老二，老三, .. ,老六, 小七.

```javascript
var arr = ['Liu laoda', 'Li laoer', 'Wei laosan', 'Frank', 'Guan laowu', 'Yang laoliu', 'Li xiaoqi'];

// 在 forEach 中 传入 一个function ，接收两个参数
arr.forEach(function(name,index){
  console.log(name, ' - ',index);
})

```

结果如下： 第一个参数就是 当前遍历的元素，第二参数为当前元素的索引

![image-20220602215640635](image/array_foreach/image-20220602215640635.png)





注意： `forEach()` 为每个数组元素执行一次 `callback` 函数  ，即每个元素都会执行一次`callback` 函数 







来看看回调函数的第三个参数 ，表示 就是这个数组本身。

```javascript
var arr = ['Liu laoda', 'Li laoer', 'Wei laosan'];

arr.forEach(function(name,index,person){
  console.log(name, ' - ',index);
  console.log(person);
})
```



![image-20220602220932068](image/array_foreach/image-20220602220932068.png)







还有一个参数 `thisArg` 这个参数

当回调函数执行的时候， 回调函数中 使用this 就是这个值。来看一个例子

```javascript
var arr = ['Liu laoda', 'Li laoer', 'Wei laosan'];

// thisArg 此时传入  {'name':'frank'},当回调函数执行的时候，this 就是这个值。
arr.forEach(function(name,index){
  console.log(this);
},{'name':'frank'})

```



![image-20220602221446495](image/array_foreach/image-20220602221446495.png)



如果省略了 `thisArg` 参数，或者其值为 `null` 或 `undefined`，`this` 则指向全局对象。



```js
var arr = ['Liu laoda', 'Li laoer', 'Wei laosan'];
// 没有传thisArg 参数
arr.forEach(function(name,index){
  // 此时是window
	console.log(this);
})

```



![image-20220602221819234](image/array_foreach/image-20220602221819234.png)









## 使用forEach注意事项

 除了抛出异常以外，没有办法中止或跳出 `forEach()` 循环。如果你需要中止或跳出循环，`forEach()` 方法不是应当使用的工具。

forEach 不支持 break这种语句退出循环。





如果你想在遍历数组的过程中想要 提前终止循环，就不要使用 forEach 遍历， 可以使用for 循环来遍历数组.

例如：我只想遍历到i==0 的时候，提前终止循环

```js

var arr = ['Liu laoda', 'Li laoer', 'Wei laosan'];

for (let i = 0; i < arr.length; i++) {
    if (i === 1) {
        break;
    }
    console.log(arr[i],'-', i);
}

// Liu laoda - 0
```





## 参考文档

[mdn doc forEach](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Array/forEach)







<center>  
    <font color=gray size=1  face="黑体">
 分享快乐,留住感动.  '2022-06-02 22:30:40' --frank 
    </font>
</center>