这个仓库主要记录一些我的博客文章，包含技术，设计模式，个人思考，读书笔记等。文章主要使用markdown 编写而成。




## 文章列表


### python3 进阶系列
- [1.python中内置类型添加属性](./2022/python内置类型添加属性.md)
- [2. Python3.10 新特性](./2022/python310-new-feature.md)
- [2-1.Python3.8 新特性](./2022/python308-new-feature.md)

- [3.Python3 遍历的list 的时候 直接删除元素 注意事项 ](./2022/python-remove-ele.md)
- [4.Python3 解释器与字节码 ](./2022/python-bytecode.md)
- [5.Python3 中 三个点是啥意思?](./2022/python-three-dots.md)
- [6.Python3 中重试库 Tenacity 推荐](./2022/tenacity-describe.md)
- [7.Python3 中什么是描述符?](./2022/python-descriptor.md)
- [8.Python requirements  == 和 ~= 区别](./2023/python-requirments.md)
- [9.Python 离线安装包](./2023/offline_install_package.md)
- [10.Python 分割字符串](./2023/python_split_string.md)
- [11.Python yydict介绍](./2024/yydict_introduction.md)
- [12 python 正则分割字符串2](./2024/python-split-string-2.md)



### Flask入门到放弃系列

- [1.Flask 获取GET请求参数](./2022/flask获取请求参数.md)
- [2.Flask 获取POST请求参数](./2022/flask获取post请求参数.md)





### Javascript 入门到放弃系列

- [1.Array splice 方法介绍](./2022/javascript/array_splice.md)
- [2.Array forEach 方法介绍](./2022/javascript/array_foreach.md)
- [3.轮播图原生js实现-1](./2022/javascript/carousel.md)
- [4.轮播图原生js实现-2](./2022/javascript/carousel-II.md)




### 设计原则
- [1.设计模式-单一职责原则](./2022/pattern-desin-srp.md)
- [2.设计模式-里氏替换原则](./2022/pattern-design-lsp.md)
- [3.设计模式-开闭原则](./2022/pattern-design-ocp.md)
- [4.设计模式-接口隔离原则](./2022/pattern-design-isp.md)
- [5.设计模式-依赖倒置原则](./2022/pattern-design-dip.md)



### 读书笔记
- [1.代码整洁之道读书笔记](./2022/代码整洁之道-读书笔记.md)
- [2.天才在左疯子在右读书笔记](./2022/天才在左疯子在右-读书笔记.md)



### 面试系列
- [1.Mysql索引 BTree 与 B+Tree 的区别](./2022/interview-Mysql-Btree.md)



### MySQL8.0x新增特性
- [1.MySQL8.0 新特性 账户与安全](./2022/MySQL8/001-acount_and_security.md)
- [2.MySQL8.0 索引优化-invisible index ](./2022/MySQL8/002-mysql-index.md)
- [3.MySQL8.0 索引优化-descending index ](./2022/MySQL8/003-mysql-index-descending.md)
- [4.MySQL8.0 索引优化-functional index ](./2022/MySQL8/004-mysql-index-functional.md)



### Linux 命令

- [1.tar 命令](./2023/linux_command_tar.md)
- [2.split 命令](./2023/linxu_command_split.md)
- [3.nc 命令](./2025/LinuxShell/linux_command_nc.md)
- [4.检测服务端口是否开放的常用方法](./2025/LinuxShell/检测服务端口是否开放的常用方法.md)


### Jenkins CICD 
- [1.Jenkins 安装与配置](./2025/Jenkins笔记/001_jenkins搭建.md)
- [2.Jenkins Pipeline 基础语法](./2025/Jenkins笔记/002_jenkins_Pipeline基础语法.md)
- [3.Jenkins Pipeline 创建任务](./2025/Jenkins笔记/003_jenkins_Pipeline创建任务.md)


### docker 相关
- [1.docker root dir 迁移](./2023/docker/docker-rootdir-迁移.md)
- [2. docker-compose file depends_on](./2023/docker/docker_depends_on.md)



### git 相关

- [1.git 标签管理](./2023/git-标签管理.md)
- 



### Redis 相关应用
- [1.Redis固定窗口限流](./2024/Redis实现简单限流.md)

### 开发工具
- [pycharm配置快捷打开方式](./2023/pycharm配置.md)


### 生活笔记 
- [2024总结](./2024/2024总结.md)