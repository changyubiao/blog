# yaml语言学习

最近发现在学习k8s中各种配置文件 都是使用的yaml 这种格式,  包括 `docker-compose.yaml` 也都是用这个格式配置部署项目信息,我就了解了一下这个语法就有了这篇文章.





## yaml 简介

YAML 是 "YAML Ain't a Markup Language"（YAML 不是一种标记语言）的递归缩写。,它是一种高可读的数据序列号语言, 可以被绝大数编程语言支持使用, 主要用于数据序列号,配置文件等.

在开发的这种语言时，YAML 的意思其实是："Yet Another Markup Language"（仍是一种标记语言）。

数据序列化就是可以高效的表示或描述数据以及数据关系的,以便用于储存和传输.



优点: 

- 语法简单
- 结构清晰,易于阅读
- 功能丰富,可以描述比JSON更加复杂的结构







## yaml 和json 区别

1. yaml 中 可以添加注释的.json 是不支持添加注释 
2. yaml 中有锚点和引用的概念, 而json 中没有.





## 基本语法

- 大小写敏感
- 使用缩进表示层级关系, 缩进必须是空格
- 缩进的空格数不重要，只要相同层级的元素左对齐即可
- `#` 表示注释



缩进数量不重要, 一般设为2个,或者4个都可以. 只是表示层级关系.

```yaml
app:
  name: erveryday
  server: 
    host: bb.com
    port: 3338
    ip: 11.11.22.33

```



```json
{
  "app": {
    "name": "erveryday",
    "server": {
      "host": "bb.com",
      "port": 3338,
      "ip": "11.11.22.33"
    }
  }
}
```





```yaml
spring:
  datasource:
    driveClassName: com.mysql.jdbc.Driver
    url: aabb.com
    username: test
    password: xxxxxx-xxx-xxx
	type: com.alibaba.Database
    
```



转换后JSON

```json
{
  "spring": {
    "datasource": {
      "driveClassName": "com.mysql.jdbc.Driver",
      "url": "aabb.com",
      "username": "test",
      "password": "xxxxxx-xxx-xxx"
    }
  },
  "type": "com.alibaba.Database"
}
```





## 数据类型

YAML 支持以下几种数据类型：

- 对象：键值对的集合，又称为映射（mapping）/ 哈希（hashes） / 字典（dictionary）
- 数组：一组按次序排列的值，又称为序列（sequence） / 列表（list）
- 纯量（scalars）：单个的、不可再分的值



### YAML 对象

对象键值对使用冒号结构表示 **key: value**， 注意: 冒号后面要加一个空格。

也可以使用 **key:{key1: value1, key2: value2, ...}**。

对象中也是可以嵌套对象的.

```yaml
app: erveryday
server:
  host: bb.com
  port: 3338
  ip: 11.11.22.33
```



对应的json 文件如下:

```json
{
  "app": "erveryday",
  "server": {
    "host": "bb.com",
    "port": 3338,
    "ip": "11.11.22.33"
  }
}
```





### YAML 数组

以  `-` 开头的行表示构成一个数组： 

注意 `-` 后面有一个空格

```
- A
- B
- C

persons:
  - frank
  - xiaoming
  - zhangsan
  
```



```yaml
fruits:
  - apple
  - orange
  - banana
  - pear 
```



转换为JSON对应

```json
{
  "fruits": [
    "apple",
    "orange",
    "banana",
    "pear"
  ]
}
```





### 锚点和引用 

定义锚点 可以使用锚点的数据

`&` 用来建立锚点（defaults），`<<` 表示合并到当前数据，`*` 用来引用锚点。

```yaml 
spring:
  datasource:
    dev:
      driveClassName: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306
      username: dev
      password: xxxxxx-xxx-xxx
      type: com.alibaba.Database
	
    test:
      driveClassName: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://localhost:3306
      username: test
      password: xxxxxx-xxx-xxx
      type: com.alibaba.Database

    prod:
      driveClassName: com.mysql.cj.jdbc.Driver
      url: jdbc:mysql://aaa.bbb.com:3306
      username: prod
      password: xxxxxx-xxx-xxx
      type: com.alibaba.Database
```

比如上面 `driveClassName` 都是一样的, 我需要写三遍, 这个时候 可以使用锚点来解决这个问题.



定义一个`&` +  `ClassName` 作为一个锚点, 引用的时候 使用 `*ClassName`  来引用锚点的值,  其中 ClassName 就是锚点名称.

```yaml
spring:
  datasource:
    dev:
      driveClassName: &ClassName com.mysql.cj.jdbc.Driver 
      url: jdbc:mysql://localhost:3306
      username: dev
      password: xxxxxx-xxx-xxx
      type: com.alibaba.Database
	
    test:
      driveClassName: *ClassName
      url: jdbc:mysql://localhost:3306
      username: test
      password: xxxxxx-xxx-xxx
      type: com.alibaba.Database

    prod:
      driveClassName: *ClassName
      url: jdbc:mysql://aaa.bbb.com:3306
      username: prod
      password: xxxxxx-xxx-xxx
      type: com.alibaba.Database
```







第二种方式引用, 定义个 默认配置信息,  `&default` 这是一个锚点,然后 需要吧这个信息 放到 下面的配置段 中, 同时 我不希望有key 值, 可以使用 `<<` 表示连接引用, 后面使用 `*default`  即可.

```yaml
# 定义 default 锚点
defaults: &defaults
  driver: mysql
  host:   localhost

dev:
  database: dev
  default: *defaults

test:
  database: test
  <<: *defaults

prod:
  database: prod
  <<: *defaults
```



```json

{
  "defaults": {
    "driver": "mysql",
    "host": "localhost"
  },
  "dev": {
    "database": "dev",
    "driver": "mysql",
    "host": "localhost"
  },
  "test": {
    "database": "test",
    "driver": "mysql",
    "host": "localhost"
  },
  "prod": {
    "database": "prod",
    "driver": "mysql",
    "host": "localhost"
  }
}
```











### 纯量

纯量是最基本的，不可再分的值，包括：

- 字符串
- 布尔值
- 整数
- 浮点数
- Null
- 时间
- 日期

```yaml
boolean: 
    - TRUE  #true,True都可以 
    - FALSE  #false，False都可以
float:
    - 3.14
    - 6.8523015e+5  #可以使用科学计数法
int:
    - 123
    - 0b1010_0111_0100_1010_1110    #二进制表示
null:
    nodeName: 'node'
    parent: ~  #使用~表示null
string:
    - 哈哈
    - 'Hello world'  #可以使用双引号或者单引号包裹特殊字符
    - newline
      newline2    #字符串可以拆成多行，每一行会被转化成一个空格
date:
    - 2018-02-17    #日期必须使用ISO 8601格式，即yyyy-MM-dd
datetime: 
    -  2018-02-17T15:02:31+08:00    #时间使用ISO 8601格式，时间和日期之间使用T连接，最后使用+代表时区
```





## 参考文档

在线验证工具 https://www.bejson.com/validators/yaml_editor/

转化工具jsontoyaml https://www.bejson.com/json/json2yaml/

https://zhuanlan.zhihu.com/p/644393484

https://blog.csdn.net/weixin_44896406/article/details/120916357

菜鸟教程 https://www.runoob.com/w3cnote/yaml-intro.html

yaml  引用语法  https://curder.github.io/yaml-study/guide/quote.html







<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2024-01-25 20:27:31' --frank 
    </font>
</center>
