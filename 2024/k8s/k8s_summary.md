





## 第一节 K8S的基础概念

`Node`  可以理解为一个物理机或者虚拟机 ,K8S中的一个节点 ,每个节点包含Pod 所需的服务, 这些节点 是由 Control Plane 来管理, 节点的名称 用来标识Node 对象,名称必须是唯一的. Node上有三个组件分别是 kubelet ,kube-proxy,container-runtime .  

kubelet 负责管理和维护每个节点上的Pod,并确保他们按照预期运行. kubelet 是在每个节点上运行的主要 “节点代理” 它可以使用以下方式之一向 API 服务器注册：

- 主机名（hostname）
- 覆盖主机名的参数
- 特定于某云驱动的逻辑

kubelet 是基于 PodSpec 来工作的。每个 PodSpec 是一个描述 Pod 的 YAML 或 JSON 对象。 kubelet 接受通过各种机制（主要是通过 apiserver）提供的一组 PodSpec，并确保这些 PodSpec 中描述的容器处于运行状态且运行状况良好。 kubelet 不管理不是由 Kubernetes 创建的容器。

容器运行时 , 有很多种, 比如 docker-engine , [containerd](https://kubernetes.io/zh/docs/setup/production-environment/container-runtimes/#containerd) , [CRI-O](https://kubernetes.io/zh/docs/setup/production-environment/container-runtimes/#cri-o)  等



控制平面（Control Plane）是指容器编排层，它暴露 API 和接口来定义、 部署容器和管理容器的生命周期。



`Pod` 是kubernetes 的最小调度单元, 他是 一个或多个容器的组合 . 一般只有一个容器,可以理解为容器的抽象.一个 Pod 可以包含一个或多个紧密关联的容器， 它们共享相同的网络命名空间、 IP 地址和存储卷， 并在同一个宿主上运行。



`Service` 是将运行在一个或一组 Pod 上的网络应用程序公开为网络服务的方法。



Ingress 是对集群中服务的**外部访问**进行管理的 API 对象， 典型的访问方式是 HTTP。 可以通过Ingress资源来配置不同的转发规则， 从而达到根据不同的规则设置访问集群内不同的Service所对应的后端Pod。



命名空间 （ namespace） 提供一种机制， 将同一集群中的资源划分为相互隔离的组， 以便进行分类、 筛选和管理。同一命名空间内的资源名称要唯一， 但跨命名空间时没有这个要求。



ConfigMap , Secret      可以把外部信息 ,敏感信息 存起来. 更新这些配置会比较方便.  避免配置变更重新编译和部署的问题.



Volumes  可以将数据挂载到集群中本地磁盘和远程存储上, 做数据持久化使用的.



Deployment  组件 来管理无状态的组件, 可以将一个或多个Pod 组合在一起, 副本控制, 滚动更新, 动态扩缩容等 等. 



StatefulSet 组件  来管理 有状态的组件 (mysql ,缓存,消息队列等) 

对于有状态的应用程序, 可以单独部署, 从 K8S 集群中剥离出来, 单独部署. 





<center>
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2024-01-21 21:39:18' --frank
    </font>
</center>










##  第六节 Portainer来管理集群 

下载配置文件

```
wget  https://downloads.portainer.io/ce2-19/portainer.yaml 
```



或者直接使用 网络上的文件

```
kubectl apply -n portainer -f https://downloads.portainer.io/ce2-19/portainer.yaml
```

```
kubectl  apply -f portainer.yaml -n portainer
```





-n  用来指定命名空间的

```
kubectl get all -n portainer 
```







```bash
# 获取主节点的IP 
kubectl get nodes -o wide

ubuntu@k3s:~$ kubectl get nodes -o wide
NAME      STATUS   ROLES                  AGE     VERSION        INTERNAL-IP    EXTERNAL-IP   OS-IMAGE             KERNEL-VERSION      CONTAINER-RUNTIME
k3s       Ready    control-plane,master   3d20h   v1.28.5+k3s1   192.168.64.2   <none>        Ubuntu 22.04.3 LTS   5.15.0-91-generic   containerd://1.7.11-k3s2
worker1   Ready    <none>                 3d20h   v1.28.5+k3s1   192.168.64.4   <none>        Ubuntu 22.04.3 LTS   5.15.0-91-generic   containerd://1.7.11-k3s2
worker2   Ready    <none>                 3d20h   v1.28.5+k3s1   192.168.64.5   <none>        Ubuntu 22.04.3 LTS   5.15.0-91-generic   containerd://1.7.11-k3s2
```





访问 http://192.168.64.2:30777/#!/init/admin

```reStructuredText
user:admin
password:Password#0121
```







安装  kubectl 

https://kubernetes.io/zh-cn/docs/tasks/tools/install-kubectl-macos/#install-with-homebrew-on-macos

安装kind 

https://kind.sigs.k8s.io/docs/user/quick-start/



