## 第二节 K8S 的架构

K8S 架构图如下: 

官方文档: https://kubernetes.io/docs/concepts/architecture/

![image-20240107170645677](image/02-k8s-architecture/image-20240107170645677.png)





![image-20240122205709292](image/02-k8s-architecture/image-20240122205709292.png)





Control Plane 控制平面（Control Plane）是指容器编排层，它暴露 API 和接口来定义、 部署容器和管理容器的生命周期,也叫 Master 节点



在控制平面中有 5个组件  etcd, api-server , scheduler, Controller  Manager and  Cloud Controller Manager 



kube-api-server 是集群的核心， 是k8s中最重要的组件， 因为它是实现声明式api的关键, 整个集群的入口,所有请求都要经过它, api接口服务.  kubernetes api-server的核心功能是提供了Kubernetes各类资源对象 （ pod**、** RC **、**service等）的增、 删、 改、 查以及watch等HTTP REST接口. 



kube-controller-manager 的作用简而言之： 保证集群中各种资源的实际状态（ status） 和用户定义的期望状态 （ spec） 一致。 官方定义： kube-controller-manager 运行控制器，它们是处理集群中常规任务的后台线程。当集群Pod/Service 出现故障的时候,会做出相应的相应.



kube-scheduler 是kubernetes 系统的核心组件之一， 主要负责整个集群资源的调度功能， 根据特定的调度算法和策略， 将Pod 调度到最优的工作节点上面去， 从而更加合理、 更加充分地利用集群的资源。



etcd 是兼具一致性和高可用性的键值数据库， 可用于服务发现以及配置中心。 采用raft一致性算法， 基于Go语言实现。 是保存Kubernetes 所有集群数据的后台数据库， 在整个云原生中发挥着极其重要的作用。



cloud-controller-manager 是指云控制器管理器， 一个 Kubernetes 控制平面组件， 嵌入了特定于云平台的控制逻辑。 云控制器管理器允许你将你的集群连接到云提供商的 API 之上， 并将与该云平台交互的组件同与你的集群交互的组件分离开来。





Node节点, 集群的数据平面，负责为容器提供运行环境

`kubelet` : 负责维护Pod的生命周期,存储和网络

`kube-proxy` : 负责提供集群内部的服务发现和负载均衡,网络代理

`docker run time` :  负责给容器提供运行时环境.





https://znunwm.top/archives/k8s-xiang-xi-jiao-cheng

https://znunwm.top/archives/121212#5.3-pod%E7%94%9F%E5%91%BD%E5%91%A8%E6%9C%9F



<center>
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2024-01-22 21:13:54' --frank
    </font>
</center>


