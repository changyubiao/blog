



# Python 中re.split的使用



`re.split` 是 Python 中的一个正则表达式函数，用于根据一个或多个匹配模式来分割字符串。它返回的是一个列表，其中包含了按照模式分割后的各个子字符串。

#### 函数签名

```python
re.split(pattern, string, maxsplit=0, flags=0)
```

- **pattern**: 正则表达式的模式，用于确定如何进行分割。
- **string**: 需要被分割的字符串。
- **maxsplit** (可选): 指定最大分割次数，默认值为 0，意味着不限制分割次数。
- **flags** (可选): 编译标志，例如 `re.IGNORECASE` 表示忽略大小写。

#### 示例

 **示例 1: 基本使用**

假设我们有一个字符串，我们想根据空格将其分割成单词。

```python
import re

text = "Hello world this is a test"
result = re.split(r'\s+', text)
print(result)  # 输出: ['Hello', 'world', 'this', 'is', 'a', 'test']
```

这里 `\s+` 匹配一个或多个空白字符（包括空格、制表符等）。

**示例 2: 使用 maxsplit 参数**

如果我们只想分割前两次出现的空格：

```python
result = re.split(r'\s+', text, maxsplit=2)
print(result)  # 输出: ['Hello', 'world', 'this is a test']
```

**示例 3: 忽略大小写**

如果模式中包含大小写敏感的数据，可以使用 `flags` 参数来忽略大小写：

```python
text = "Red, blue, GREEN, yellow"
result = re.split(r'blue|green', text, flags=re.IGNORECASE)
print(result)  # 输出: ['Red, ', ', ', 'yellow']
```

**示例 4: 使用括号捕获分组**

如果需要**保留匹配结果**，可以使用分组

如果在模式中使用了括号 `( )`，那么这些括号内的匹配结果也会出现在结果列表中：

```python
text = "123-456-7890"
result = re.split(r'(\d{3})-(\d{3})-(\d{4})', text)
print(result)  # 输出: ['', '123', '456', '7890', '']
```

注意，这个例子中的输出包含了一些空字符串，因为 `-` 也被视为分割点，但它没有被捕获到括号内。

**示例 5: 分割并过滤掉空字符串**

有时候，分割操作可能会产生一些空字符串，这可以通过列表推导来过滤掉：

```python
text = ",,,abc,,def,,ghi,"
result = [item for item in re.split(r',+', text) if item]
print(result)  # 输出: ['abc', 'def', 'ghi']
```

这些是 `re.split` 函数的一些基本用法和高级特性。





有的时候 我们分割的字符串，需要保留 在字符串列表中， 我们需要再进一步处理，比如 添加一些属性信息，样式说明信息等

比如下面的文本 我希望在中括号里的东西 单独分割出来 ，然后单独处理这些字符，加属性或者其他的内容， 这个时候可以使用括号作为分组 ，来保留匹配度内容



```python
import re

text = "This is a test [one] and another 【two】 and yet another [three] and finally 【four】"

#匹配中括号, 或者中文中括号里面的内容
pattern = r'(\[\w+\]|【\w+】)'

# 模式中的括号表示捕获分组，这样分组内的内容也会出现在结果列表中
result = re.split(pattern, text)
print(result)  # ['This is a test ', '[one]', ' and another ', '【two】', ' and yet another ', '[three]', ' and finally ', '【four】', '']
recover_res = ''.join(result)
print(text == recover_res)  # True
```

结果成功把结果分割出来了。 

| `\w` | 匹配字母、数字、下划线。等价于 `[A-Za-z0-9_]`。    |
| ---- | -------------------------------------------------- |
| `\W` | 匹配非字母、数字、下划线。等价于 `[^A-Za-z0-9_]`。 |



之前我写过一篇文章  [python 正则分割字符串](https://blog.csdn.net/u010339879/article/details/135472219) 

就是用正则来处理字符串 非常麻烦， 现在可以通过这样的方法 ，通过添加**分组**来**保留被分割**的部分 。 之后在对字符串 进一步的处理。

```python
import re
import unittest



def time_split(text: str) -> list:
    # 使用正则分割 字符串
    split_res = re.split(r'(\d{1,2}:\d{2})', text)
    # 去除开头空字符串
    split_res = [item for item in split_res if item]

    result = []
    for i in range(0, len(split_res), 2):
        item = split_res[i] + split_res[i + 1]
        result.append(item)
    return result


class TestSplitStr(unittest.TestCase):

    def setUp(self):
        self.s = '14:00,中国科技大学,KZB 阶段总结推进会-贺董(周总、沈总)15:00,中国科技大学,公司深改领导小组会(视频)班子成员16:30,中国科技大学,C929供应商选择领导小组会-贺董(周总、张总、沈总、戚总)18:30,中国科技大学,国内大飞机产业链布局方案专题会议-周总(张总、沈总)'

        self.expected_result = [
            '14:00,中国科技大学,KZB 阶段总结推进会-贺董(周总、沈总)',
            '15:00,中国科技大学,公司深改领导小组会(视频)班子成员',
            '16:30,中国科技大学,C929供应商选择领导小组会-贺董(周总、张总、沈总、戚总)',
            '18:30,中国科技大学,国内大飞机产业链布局方案专题会议-周总(张总、沈总)'
        ]

    def test_time_split(self):
        real_result = time_split(self.s)
        self.assertListEqual(real_result, self.expected_result, msg="real != expected result.Test failed...")


if __name__ == '__main__':
    unittest.main()

```





#### 总结

​        本文处理文档的小技巧 通过 re.split 正则来分割字符串 转成 列表 ， 保留正则匹配的部分。 这样方便，高效，不要在写很多的代码来处理逻辑。希望可以给你也带来一些启发。 







#### 参考文档

Python3 元字符  https://www.runoob.com/regexp/regexp-metachar.html





<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2024-11-25 23:21:26' --frank 
    </font>
</center>




