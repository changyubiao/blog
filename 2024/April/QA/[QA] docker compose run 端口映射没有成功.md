# [[QA\] docker compose run 端口映射没有成功?](https://wiki.datagrand.com/pages/viewpage.action?pageId=236749168)











Q: docker compose file 中端口无法映射 好像没有生效？

我在 docker-compose.yml 文件中定义了两个服务 ， web 和 mysqldb 这两个服务，

分别做了端口的映射。

**docker-compose** 展开源码

```yaml
# docker-compose.yml
version: '3.8'
 
services:
 
  web:
    container_name: 'spider-dev'
    build:
      context: .
 
    environment:
      PYTHONUNBUFFERED: 1
 
    depends_on:
      - mysqldb
    ports:
      - 8000:5000
    volumes:
      - ./:/app
 
  
  mysqldb:
    container_name: mysql8
    image: mysql:8.0.32
    hostname: mysql-master
    user: root
    command:
      --default-authentication-plugin=mysql_native_password
      --character-set-server=utf8mb4
      --collation-server=utf8mb4_unicode_ci
 
    environment:
      LANG: C.UTF-8
      MYSQL_ROOT_PASSWORD: pwd
      MYSQL_DATABASE: yinlian
     
    ports:
      - "3330:3306"
 
 
    volumes:
      - "./docker/db/data:/var/lib/mysql"
      - "./docker/db/mysql-files:/var/lib/mysql-files"
      - "./docker/db/my.cnf:/etc/mysql/conf.d/my.cnf"
 
    healthcheck:
      test: [ "CMD", "mysqladmin" ,"ping", "-h", "localhost" ]
      interval: 60s
      timeout: 5s
      retries: 3
```









正常来启动 docker compose 文件
docker compose up -d

通过查看 docker ps 看起来都非常正常。



```yaml

 docker ps
CONTAINER ID   IMAGE               COMMAND                   CREATED         STATUS                            PORTS                               NAMES
248b0e7eeaab   spider-web   "python3 app.py"          3 seconds ago   Up 1 second                       0.0.0.0:8000->5000/tcp              heimao-spider-dev
5a2ca45c8372   mysql:8.0.32        "docker-entrypoint.s…"   3 seconds ago   Up 2 seconds (health: starting)   33060/tcp, 0.0.0.0:3330->3306/tcp   mysql8

```


本地连接数据库也是非常正常。

停掉服务



```
docker compose down
```





突然有一天 我只想单独启动一个服务 MySQLdb

```
docker compose -f docker-compose.yml run -d mysqldb
```





发现可以起来, 但是本地尝试连接数据库失败。 查看进程状态 发现 端口没有映射起来。

```

 docker  ps
CONTAINER ID   IMAGE          COMMAND                   CREATED              STATUS                        PORTS                 NAMES
4184142e1ffa   mysql:8.0.32   "docker-entrypoint.s…"   About a minute ago   Up About a minute (healthy)   3306/tcp, 33060/tcp   heimao_spider-mysqldb-run-8e95c07f1b64

```



我尝试使用本地连接 MySQL server ， 我发现不能连上 。







后面查看了官方文档，才知道 run 需要单独做端口映射的。 
来看下原文 [compose_run](https://docs.docker.com/engine/reference/commandline/compose_run/)

The second difference is that the 
docker compose run command does not create any of the ports specified in the service configuration. This prevents port collisions with already-open ports. If you do want the service’s ports to be created and mapped to the host, specify the 
--service-ports



```
docker compose -f docker-compose.yml run --service-ports -d mysqldb
```

这样起来后 发现可以正常运行起来了



![img]("image/[QA] docker compose run 端口映射没有成功/image2023-6-29_11-18-12.png")



**参考链接**
https://docs.docker.com/engine/reference/commandline/compose_run/