# [[QA\] docker compose has no container to start](https://wiki.datagrand.com/display/~changyubiao/[QA]+docker+compose+has+no+container+to+start)









docker-compose.yml 文件如下: 



**docker-compose.yml** 折叠源码

```yaml
version: '3'
 
services:
   
 
  selenium:
    container_name: 'selenium'
    image: selenium/standalone-chrome
    restart: always
    ports:
      - "4444:4444"
 
    networks:
      - app-network
 
    healthcheck:
      test: [ "CMD", "curl", "-f", "http://localhost:4444/wd/hub/status" ]
      interval: 30s
      timeout: 5s
      retries: 3
 
 
networks:
  app-network:
    driver: bridge
```





```bash
docker compose -f docker-compose.yml   start  selenium
 
# 报错如下
service "selenium" has no container to start
```





A: 这种情况 是因为 对应 service 的容器 被删除了，需要重新创建容器. 



The issue here is that you haven't actually created the containers. You will have to create these containers before running them. You could use the `docker compose up` instead, that will create the containers and then start them.

Or you could run `docker compose create` to create the containers and then run the `docker compose start` to start them.





可以重新 执行 up ,如果没有容器 会重建容器. 



 折叠源码

```
docker compose -f docker-compose.yml  up -d  selenium
```















参考文档:

https://stackoverflow.com/questions/39562748/docker-compose-start-error-no-containers-to-start