





用docker 环境来部署 项目代码的时候， 发现 使用print 语句 通过  `docker logs -n 100 container_id ` 发现看不到日志。









在 docker-compose.yml 文件中 添加 这样的一个选项

 折叠源码

```
environment:
  PYTHONUNBUFFERED: 1
```







关于 PYTHONUNBUFFERED 的解释 

在 Python 中，PYTHONUNBUFFERED 是一个环境变量，用于控制 Python 解释器是否对标准输出和标准错误进行缓冲。
当 PYTHONUNBUFFERED 环境变量设置为 1 时，Python 解释器将不会对输出进行缓冲，而是直接将输出立即写入到标准输出和标准错误流中。
这意味着，输出会立即显示在终端上，而不需要等到缓冲区被填满或者程序执行完毕才显示。这对于实时输出信息、调试和日志记录非常有用。
例如，在使用 Python 脚本时，如果想要实时查看输出结果，可以使用 PYTHONUNBUFFERED=1 python script.py 命令来运行脚本，这样输出将会立即显示在终端上。如果不设置 PYTHONUNBUFFERED 环境变量或者设置为 0，则会启用默认的输出缓冲机制，输出将不会立即显示在终端上。









参考资料 

docker 日志输出问题
https://stackoverflow.com/questions/65014519/docker-log-dont-show-python-print-output