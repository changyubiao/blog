









 [Dockerfile 一些命令区别](https://wiki.datagrand.com/pages/viewpage.action?pageId=236750099)





# Dockerfile RUN，CMD，ENTRYPOINT命令区别



Dockerfile中RUN，CMD和ENTRYPOINT都能够用于执行命令，下面是三者的主要用途：
RUN命令执行命令并创建新的镜像层，通常用于安装软件包
CMD命令设置容器启动后默认执行的命令及其参数，但CMD设置的命令能够被docker run命令后面的命令行参数替换
ENTRYPOINT配置容器启动时的执行命令（不会被忽略，一定会被执行，即使运行 docker run时指定了其他命令）
————————————————
原文链接：https://blog.csdn.net/qq_33495762/article/details/106503418





参考文档

官方文档
https://docs.docker.com/engine/reference/builder/#cmd
https://docs.docker.com/engine/reference/builder/#entrypoint
https://docs.docker.com/engine/reference/builder/#run