





[转至元数据结尾](https://wiki.datagrand.com/display/~changyubiao/Pypi+build#page-metadata-end)

- 由 [常玉标](https://wiki.datagrand.com/display/~changyubiao)创建于[十二月 24, 2023](https://wiki.datagrand.com/pages/viewpreviousversions.action?pageId=334266528)

[转至元数据起始](https://wiki.datagrand.com/display/~changyubiao/Pypi+build#page-metadata-start)





## 构建命令

```
python3 -m pip install build

python -m build  --sdist  --wheel 

# 默认选项 
python -m build  
```







上传pypi 到

```
twine upload --repository testpypi dist/*

 
 
twine upload --repository pypi dist/*

```







`~/.pypirc` 在用户家目录下配置这个. token 从 pypi 登录之后 创建一个token 即可.

```
[distutils]
index-servers=
   testpypi
   pypi

[pypi]
  repository: https://upload.pypi.org/legacy/
  username = __token__
  password = pypi-AgEIcHlwaS5vcmcCJDU1NjhmZDdiLWU0Y2EtNGI2MS04MTZhLWNmNDBkOWZkMGU1YwACKlszLCIyNTlkNDI5My1iNzA0LTRmM2QtYjA2Ny00ZWNhM2U3MzU4M2YiXQAABiAtN5en-oDNeUAW_PslRk2DWrV0_uQ30InDA7L2cS7dwA


[testpypi]
  repository: https://test.pypi.org/legacy/
  username = __token__
  password = pypi-AgENdGVzdC5weXBpLm9yZwIkNDM0OTJjOTYtOTRjNi00ZGQ4LWE1MjctODJjYzVhODZmNjUwAAIqWzMsIjI2YTkxZmZhLWIzMmMtNGM4Ni05NGM2LTBiMWVjZTYzYmJiNCJdAAAGIIvYdOXaNNqVnK5f_bvl539aId_GqhwMEOC7Pw38RlvD

```







参考文档:

https://setuptools.pypa.io/en/latest/userguide/quickstart.html#uploading-your-package-to-pypi

https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#create-an-account



配置pypirc 文件

https://packaging.python.org/en/latest/specifications/pypirc/#pypirc

package 命名规则

https://packaging.python.org/en/latest/guides/distributing-packages-using-setuptools/#standards-compliance-for-interoperability

语义化版本原则

https://semver.org/lang/zh-CN/