





[转至元数据结尾](https://wiki.datagrand.com/pages/viewpage.action?pageId=236749143#page-metadata-end)

- 由 [常玉标](https://wiki.datagrand.com/display/~changyubiao)创建, 最后修改于[六月 29, 2023](https://wiki.datagrand.com/pages/diffpagesbyversion.action?pageId=236749143&selectedPageVersions=3&selectedPageVersions=4)

[转至元数据起始](https://wiki.datagrand.com/pages/viewpage.action?pageId=236749143#page-metadata-start)





docker compose 的 使用
https://docs.docker.com/compose/gettingstarted/



命令行使用
https://docs.docker.com/engine/reference/commandline/compose_ls/
https://docs.docker.com/engine/reference/commandline/compose_logs/



compose file 介绍
The Compose file 组成部分
https://docs.docker.com/compose/compose-file/01-status/



docker compose 学习地址 示例
https://github.com/docker/awesome-compose/tree/master
https://github.com/docker/awesome-compose/blob/master/official-documentation-samples



docker compose yml 文件指令详解
https://github.com/compose-spec/compose-spec/blob/master/spec.md#restart