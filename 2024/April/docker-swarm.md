





- 由 [常玉标](https://wiki.datagrand.com/display/~changyubiao)创建, 最后修改于[七月 28, 2023](https://wiki.datagrand.com/pages/diffpagesbyversion.action?pageId=256902505&selectedPageVersions=2&selectedPageVersions=3)

[转至元数据起始](https://wiki.datagrand.com/pages/viewpage.action?pageId=256902505#page-metadata-start)



在学习部署 达观助手的过程中, 使用 swarm 方式部署的,之前没有用过这种方式,所以写个笔记记录一下.

在 docker swarm 模式下, 我们一般如何查看 服务状态 ,如果查看日志等一些常使用的命令记录



**常用命令** 折叠源码

```bash
# 查看 stack``docker stack ``ls` `

# 删除 stack``docker stack ``rm` `<stack_name>
```



2 查看 stack 进程情况 

 折叠源码

```bash
docker stack ps doc-assistant
```







3 查日志 某一个task 日志

```
docker service logs  -f <task_id>
```





4 动态查看 service 日志



```
docker service logs -f <ID>

```





5 查看stack 中所有的service

docker stack services <stack_name>

```
docker stack services doc-assistant
```