





# [如何查看CPU架构](https://wiki.datagrand.com/pages/viewpage.action?pageId=271746202)





如何查看cpu 架构

```
# 直接返回cpu 架构``arch
```





查看内核版本

```
cat /proc/version

```





查看内核所有 的信息

```
uname -a

```



只查看内核名称

```
uname -r

```



## 查看 Linux 系统是 centos, 还是ubuntu ?



方法1 :

radhat或centos存在：`/etc/redhat-release` 这个文件



```
cat` `/etc/redhat-release

```



ubuntu存在 : `/etc/lsb-release` 这个文件

```
cat /etc/lsb-release
```







方法2 :

根据系统的包管理器不同.

有 `yum` 就是 centos/redhat 系统

```
yum -h
```





有 `apt-get` 就是Ubuntu 系统

```
apt-get --help
```



