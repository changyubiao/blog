



# [在MacOS 上 配置 docker 源仓库](https://wiki.datagrand.com/pages/viewpage.action?pageId=263980212)







- 由 [常玉标](https://wiki.datagrand.com/display/~changyubiao)创建于[八月 08, 2023](https://wiki.datagrand.com/pages/viewpreviousversions.action?pageId=263980212)

[转至元数据起始](https://wiki.datagrand.com/pages/viewpage.action?pageId=263980212#page-metadata-start)

在MacOS 上 配置 docker 源 仓库,配置到国内的地址



在docker 配置文件中添加 国内的镜像即可

```
{
  "registry-mirrors": [
    "https://hub-mirror.c.163.com",
    "https://mirror.baidubce.com",
     "https://mirror.baidubce.com"
  ]
}
```







使用 `docker info` 查看是否生效



```
 Registry Mirrors:
  http://hub-mirror.c.163.com/
  https://docker.mirrors.ustc.edu.cn/
 Live Restore Enabled: false
```







对于MacOS 用户

![image-20240423160643793](image/Untitled/image-20240423160643793.png)



参考文档:

dockerd 配置 https://docs.docker.com/engine/reference/commandline/dockerd/

镜像加速器 https://yeasy.gitbook.io/docker_practice/install/mirror