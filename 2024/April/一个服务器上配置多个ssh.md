







# [在一个服务器上如何配置多个ssh 呢?](https://wiki.datagrand.com/pages/viewpage.action?pageId=317653051)

[转至元数据结尾](https://wiki.datagrand.com/pages/viewpage.action?pageId=317653051#page-metadata-end)

- 由 [常玉标](https://wiki.datagrand.com/display/~changyubiao)创建, 最后修改于[十一月 09, 2023](https://wiki.datagrand.com/pages/diffpagesbyversion.action?pageId=317653051&selectedPageVersions=1&selectedPageVersions=2)

[转至元数据起始](https://wiki.datagrand.com/pages/viewpage.action?pageId=317653051#page-metadata-start)



# 在一个服务器上如何配置多个ssh 呢?



正常来说 `git clone repo.git` 使用ssh 方式克隆的时候,只要有权限 就可以正常clone. 只要把自己的public_key 放在 ssh keys 添加进去 就也可以使用了.



但有的时候 会遇见一个问题 , 在服务器上 我们很多情况使用的是公用账号. 同一个账号下面的 `~/.ssh/id_rsa.pub` 对同一个用户就是一个.



在gitlab 点击 自己的头像,选 preference, 点击User settings 下面 SSH Keys , 添加公钥的时候 ,会出现以下错误.

**Fingerprint has already been taken.**



这种错误可能是 别人已经添加了这个 id_rsa.pub 的内容了, 所以添加不上了.



![test](/Users/frank/2024/blog/2024/April/image/一个服务器上配置多个ssh/image-20231108111134226-20240424164250969.png)







添加不了 自己的public key 就不能使用 ssh 协议进行克隆了. 要么使用https 使用 用户名,密码的方式下载,每次都要输入密码,当然也可以输入一次, 之后 保存下来.

今天我想讲的内容是 如果要使用 ssh 协议进行clone, 我怎么配置多个 ssh 账号呢?

首先 我们先创建一对 密钥对

```bash
ssh-keygen  -t rsa -C "changyubiao@datagrand.com"  -f "/data/sdv1/duser/frank/.ssh/id_rsa_frank"
```

-t 参数 使用哪种算法 , 这里使用 rsa

-C 备注信息,一般可以填写邮箱

-f 指定文件生成的位置

然后在 `~/.ssh/` 创建一个名称为 `config` 文件, 如果有直接打开文件 进行修改, 如果没有直接创建文件

`config` 配置文件 添加如下配置

```bash
Host datagrand.com
   HostName git.datagrand.com
   User zhangka
   IdentityFile ~/.ssh/id_rsa


Host work
   PreferredAuthentications publickey
   HostName git.datagrand.com
   User changyubiao
   IdentityFile /data/sdv1/duser/frank/.ssh/id_rsa_frank
   IdentitiesOnly yes
```

解释一下参数含义

- Host: 这是一个自定义的**主机别名**，表示连接到名为"datagrand.com"的主机。
- HostName: 指定要连接的实际主机名为"git.datagrand.com"。
- User: 指定连接时要使用的用户名为"zhanghuan"。
- IdentityFile: 指定用于身份验证的私钥文件的路径。注意这里配置是 私钥的路径
- PreferredAuthentications: 指定首选的身份验证方法为公钥验证。
- IdentitiesOnly: 设置为"yes"，表示只使用指定的身份验证文件进行连接。

当你使用SSH客户端连接到"datagrand.com"时，会使用用户名"zhanghuan"和指定的私钥进行身份验证。

而当你连接到"work"时，会使用用户名"changyubiao"和另一个指定的私钥进行身份验证，并且只使用该私钥进行连接。

这些配置可以方便地管理多个主机和身份验证方式。

## [测试一下](https://wiki.datagrand.com/pages/viewpage.action?pageId=317653051#测试一下)

配置 完成后 , 测试 是否可以通过

```bash
# 测试之前名称
ssh -T  git@git.公司域名.com -p 58422

# 测试自己添加的名称
ssh -T  git@work -p 58422
[duser@template .ssh]$ ssh -T  git@work -p 58422
Welcome to GitLab, @changyubiao!
[duser@template .ssh]$ ssh -T  git@git.公司域名.com -p 58422
Welcome to GitLab, @zhangka!
```

到此 就配置好了, 那么我们来clone 仓库 默认复制仓库地址

```bash
git clone ssh://git@git.公司域名.com:58422/czqa/deploy_czqa_stable.git
```

此时 我们 clone 仓库 命令. `work` 就是我配置 host的一个别名, `git@自己配置的别名`

```
git clone  ssh://git@work:58422/czqa/deploy_czqa_stable.git 
```













## 参考文档

[https://drylint.com/Git/%E5%A4%9A%E4%B8%AA%E9%82%AE%E7%AE%B1%E8%B4%A6%E5%8F%B7%E5%88%9B%E5%BB%BA%E5%A4%9A%E4%B8%AAssh-key%E8%BF%9E%E6%8E%A5%E4%B8%8D%E5%90%8C%E4%BB%A3%E7%A0%81%E6%89%98%E7%AE%A1%E5%B9%B3%E5%8F%B0.html#_1-%E5%9C%A8-ssh-%E7%9B%AE%E5%BD%95%E4%B8%AD%E5%88%9B%E5%BB%BA-config-%E6%96%87%E4%BB%B6](https://drylint.com/Git/多个邮箱账号创建多个ssh-key连接不同代码托管平台.html#_1-在-ssh-目录中创建-config-文件)

https://git.datagrand.com/help/ssh/README#see-if-you-have-an-existing-ssh-key-pair

如何在一台电脑上配置多个Github SSH https://simblog.hashnode.dev/github-ssh