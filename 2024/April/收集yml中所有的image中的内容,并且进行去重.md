





# [收集 yml 中所有的image 中的内容 ,并且进行去重处理](https://wiki.datagrand.com/pages/viewpage.action?pageId=317653058)









有时候在部署仓里面 有很多镜像, 我们又不能一个个的看非常繁琐, 下面的脚步 就是搜索 compose 文件夹 下面的所有 yml 文件, 并从中获取image 镜像的名称, 并且进行去重处理. 还有删除了 注释的镜像名称, 默认写到 image.txt 文件中. 



当然 这个目录compose 可以手动 修改, 换成 yml 所在的目录即可. 



```bash
#!/bin/bash
#
# 该脚本收集 当前 ./compose 下面所有的 yml 中的镜像名称 , 默认记录到 image.txt 文件中
#
#
#
 
img_name=$1
 
#echo "first:img_name: ${img_name}"
 
if [ -z $img_name ];then
  img_name="image.txt"
fi
 
#echo "img_name: ${img_name}"
 
# exclude '#' 开头的 yml的文件内容,因为这些被注释掉了.
# look for image
find ./compose -name "*.yml" | while read -r file; do
  image=$(grep -Pv '\s*#' "$file" |grep -oP 'image:\s*\K\S+' )
  echo "$image" >> $img_name
done
 
 
 
# remove empty lines
sed -i  '/^$/d'  $img_name
 
 
# remove duplicated lines
#awk '!seen[$0]++'  $img_name  > tmp_file
awk '{ if (!seen[$0]) print $0; seen[$0]++ }'   $img_name  > tmp_file
 
mv tmp_file $img_name
```







这里是对`grep -oP 'image:\s*\K\S+' "$file"`命令的解释：

- `'image:\s*\K\S+'`是正则表达式模式，用于匹配包含`image:`后面的非空白字符的文本。

  - `image:`：匹配字面字符串`image:`。
  - `\s*`：匹配零个或多个空白字符（包括空格、制表符等）。
  - `\K`：重置匹配的起点，即忽略之前匹配到的部分。
  - `\S+`：匹配一个或多个非空白字符。

  综合起来，这个正则表达式用于匹配以`image:`开头，后跟零个或多个空白字符，然后是一个或多个非空白字符的文本。

- `"$file"`是变量，表示`find`命令找到的每个文件的路径。它将替换为实际的文件路径。

- `-o`选项表示只输出**匹配到的部分**。默认情况下，`grep`会输出整行匹配到的内容，但使用`-o`选项后，它只会输出**匹配到的部分**。

- `-P`选项启用Perl兼容的正则表达式（PCRE）。这意味着您可以使用更丰富的正则表达式语法来进行匹配。在这个特定的脚本中，我们使用了`\K`，它是PCRE的一个特殊构造，用于重置匹配结果。具体来说，`\K`会忽略之前匹配到的部分，只保留`\K`之后的内容。在我们的例子中，`\K`用于忽略`image:`这个匹配项，只保留其后面的非空白字符作为输出。