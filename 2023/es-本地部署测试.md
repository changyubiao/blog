



https://www.cnblogs.com/-mrl/p/14255595.html





把这个镜像从镜像库拉下来：

```
docker pull nshou/elasticsearch-kibana
```



最后咱们把镜像启动为容器就可以了，端口映射保持不变，咱们给这个容器命名为eskibana，到这里ES和Kibana就安装配置完成了！容器启动后，它们也就启动了，一般不会出错，是不是非常方便？节省大把时间放到开发上来，这也是我一直推荐docker的原因。



```bash
docker run -d -p 9200:9200 -p 9300:9300 -p 5601:5601 --name eskibana  nshou/elasticsearch-kibana
```







打开浏览器 5601 端口  即可看见kibanna 的界面

```
http://localhost:5601/
```

















参考文档:

创建索引

https://www.elastic.co/guide/en/elasticsearch/reference/8.7/indices-create-index.html

数据类型 

https://www.elastic.co/guide/en/elasticsearch/reference/8.7/mapping-types.html