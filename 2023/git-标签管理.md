## 10 git 标签管理

git 如何打标签呢？



### 标签是什么?

标签 相当于一个 版本管理的一个贴纸，随时 可以通过标签 切换到 这个版本的状态 ，

有人可能有疑问 `git commit` 就可以知道 代码的改动了， 为啥还需要**标签**来管理呢？

首先 多人协作开发的时候，有很多commit 有时候会比较乱，而tag 一般完成了一个release, 并且测试通过后,  再去打个 标签。 代表一次上线功能的小小的里程碑。





注意标签 是不可以 像分支一样 可以移动的.一旦打完标签就意味着 这个版本确定下来, 如果发现了这个版本的bug ,要基于这个标签 创建**分支** 进行修复,而不能直接在某个标签进行修改代码. 





### 标签和 commitID 的区别是什么呢?  

标签 本质来说 和commitID **没有区别**. 唯一的区别 标签是一个可以人类可以阅读的名称. commitID 是一个hash 值, 一般不是那么容易记住. 



可以通过标签名称 检出分支, 当然也可以通过某个 commitID 检出分支.   标签的意义 就是针对某次commit 完成了一个产品相对完整的功能,或者优化, 或者准备好了一个即将可以发布的代码版本, 我们需要记录一下这个伟大的时刻, 仅此而已. 当然 你可以说, 我通过 commit 里面的message 也是 可以说明这个 重要的提交 `git commit -m "release version"` 当然是可以的,没有问题. 如果每个人都能有commit 信息的规范做法,那么 tag的作用 就不那么大.   然而在真实开发体验中, 很多commit message, 写的相对没有那么详细, 比如:  fix a bug , Update  xxx file,  optimize code ,refactor code  等等 这些commit message. 会让代码的维护者头大,随着时间的推移, 都不知道哪个是重要的事件,或者里程碑. 这个时候 我们就需要 里程碑一样的东西, 就是对某个commitID  打标签, 标志这次这次完成了一个大的功能. 或者一次大版本测试,准备发布.  这就是 **tag** 存在的意义. 





我来演示一下根据 tag 检出分支,以及根据 commitID 检出分支 

![image-20231209143507105](/Users/frank/2023/typora_docs/blog/2023/image/git 标签管理/image-20231209143507105.png)



```bash
# 通过 tag 检出分支
git checkout -b b_2_0_tag  2.0.0  

# 通过commitID 检出分支
git checkout -b b_2_0  c1f2646
```



检出结果如下:  我们可以清晰的看到,两个分支 在同一个 commitID, 两者完全一样的.

![image-20231209150232892](/Users/frank/2023/typora_docs/blog/2023/image/git 标签管理/image-20231209150232892.png)





### 查看标签

```bash
git tag 
```

```bash
[xxxx@xxxxx xxxxx]$ git tag 
db_sharding_v0.1
db_sharding_v0.2
v20191126
v20191127
v20191127.1
v20191203
v20200119
v20200204
v20200206
```

支持 匹配查看标签

```bash
git tag  -l 'v2020*'
```

```bash
[xxxxx@xxxxxx history_retriever]$ git tag  -l 'v2020*'
v20200119
v20200204
v20200206
```

### 查看某一个标签的详情

`git show  标签名称`

```bash
git show v20200206
```

![img-git-tag-show-01](/Users/frank/2023/typora_docs/blog/2023/image/git 标签管理/48935.png)

### 打标签 并且 推送到 远程 共享标签

`git  tag -a  标签名称   -m  comment 信息 ，message 一些描述信息`

一般打标签都是针对当前的分支，最后一个提交



`-a` : 理解为 `annotated` 的首字符，表示 附注标签
`-m` : 指定附注信息

```bash
git tag -a v20200206 -m  "稳定版，release version" 

# 推送到远程 标签(共享标签)
git push origin v20200206

```

### 后期打标签

假设已经完成了某个feature 已经提交了代码，但是 忘记 打tag 了，可以通过 commit  进行打tag

```bash
# 查看提交历史
git log --pretty=oneline


# 一行显示 commit 信息 
git log    --oneline

```

```bash
[xxxxx@xxxxxx xxxxxx]$ git log --pretty=oneline
5f3f449120676310a581df86c3a58144a1da045d Merged PR 12558: test query string
3b34e0dd65fff220812a4f1858e20e1a3326b497 ut 排除
8a3a0428e4276d160c7ab220ecbf05357c17dd53 1
1892ad9d0d585bc56cd06f3c398e4b52bd902fe3 test query string
25fbee19721013c685df903cfca20517dcfea357 test query string
d6cef413a0cde8c0233ed5ff13f259bc76b9c688 Merged PR 12546: ut - rank task
0f633ea5cc0ba357b2c0cead9a1cd280c5c33a8f ut - rank task
```

### 根据 commitID 来打标签

`git tag -a 标签名称  commitID`

    git tag -a v20200109.1  0f633ea

### 删除 本地标签

`git tag -d  标签名称`

```bash
git tag -d  v0.0.1
```

### 推送一个标签
`git push origin  标签名称`

```bash
git push origin v0.0.1
```



![git-tag01](/Users/frank/2023/typora_docs/blog/2023/image/git 标签管理/49000.png)



推送本地所有的标签到远程仓库

```
git push origin --tags
```



### 删除远程仓库标签

`git push origin :refs/tags/标签名`

```bash
git push origin  :refs/tags/v0.0.1
```

![git-tag-img-del](/Users/frank/2023/typora_docs/blog/2023/image/git 标签管理/49026.png)


这个命令更加直观
```bash
 git push origin --delete <tagname>
```

### 通过标签检出一个分支

根据 标签 检出一个分支 `branch_version3`

`git checkout -b  分支名   标签名`
```bash
git checkout -b branch_version3  v3.0.0
```







## 参考链接

[git-scm.com](https://git-scm.com/book/zh/v2)

[git 标签管理](https://git-scm.com/book/zh/v2/Git-%E5%9F%BA%E7%A1%80-%E6%89%93%E6%A0%87%E7%AD%BE)





<div style="text-align:center; color:gray;font-size:10px;font-family:'黑体'; ">  
       分享快乐,留住感动.  '2023-12-09 15:07:32' --frank 
</div>
