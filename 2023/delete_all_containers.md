



# 删除所有的容器 





要删除所有的 Docker 容器，可以使用 `docker rm` 命令结合 `-f` 和 `-q` 参数。具体来说，可以使用以下命令：

```bash
docker rm -f  $(docker ps -aq)
```



这个命令的含义是：

- `docker ps -aq` 列出所有容器的 ID。
- `$(docker ps -aq)` 将容器 ID 列表作为参数传递给 `docker rm` 命令。
- `docker rm -f` 强制删除所有容器，即使它们正在运行。

总之，`docker rm -f $(docker ps -aq)` 命令将删除所有的 Docker 容器。请注意，这个命令将永久删除所有容器，因此请谨慎使用。





正常 我们可以先停掉所有的容器，再删除所有容器



停掉所有容器

```bash
docker stop  $(docker ps -q)
```



删除所有容器

```bash

docker rm $(docker ps -q)
```



 

