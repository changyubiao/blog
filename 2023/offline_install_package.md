



# 离线安装Python package 包



有时候我们在离线环境安装Python package ,然后再一台没有网络的机器上安装.

python 的包管理工具`pip` 提供了一个命令, 来帮助我们下载离线包



### pip download 参数介绍

`pip download`    

官方参考文档:  [pip_download](https://pip.pypa.io/en/stable/cli/pip_download/)

 



-d, --dest <dir>

Download packages into <dir>.



-r, --requirement <file>

Install from the given requirements file. This option can be used multiple times.





离线 下载Python 包 

```
pip download -d offline_packages -r requirements.txt
```

执行命令后 会下载 requirements.txt 的package 到 `offline_packages` 文件夹



在一台离线的机器上, 把 offline_packages 文件夹 复制到这台机器上面, 放到 项目目录,

执行下面的命令 安装离线包

```bash
pip install --no-index --find-links=./offline_packages   -r ./requirements.txt
```



`--no-index`

​		Ignore package index (only looking at --find-links URLs instead).

这个参数



`-f, --find-links <url>`

If a URL or path to an html file, then parse for links to archives such as sdist (.tar.gz) or wheel (.whl) files. If a local path or [file://](file:///) URL that’s a directory, then look for archives in the directory listing. Links to VCS project URLs are not supported.

正常来说 如果这么顺利 也就没有这篇博客了. 



### 报错: 报错信息 平台报错 

error: xxx.whl **is not supported wheel on this platform**



我是 从mac 笔记本的下载的package, 迁移到 Windows 电脑,发现 这些包 不能够安装在Windows系统上面. 因为mac 默认下载包的时候, 会默认使用当前系统环境的包 进行下载





我仔细看阅读了 [pip download](https://pip.pypa.io/en/stable/cli/pip_download/) 的官方文档, 可以指定平台信息,Python版本, 以及实现方式等. 

--only-binary <format_control>

Do not use source packages. Can be supplied multiple times, and each time adds to the existing value. Accepts either “:all:” to disable all source packages, “:none:” to empty the set, or one or more package names with commas between them. Packages without binary distributions will fail to install when this option is used on them.

这个参数表示 是否使用 二进制包 



--platform  <platform>

Only use wheels compatible with <platform>. Defaults to the platform of the running system. Use this option multiple times to specify multiple platforms supported by the target interpreter.

操作系统的信息 , 在 Python 的开发中，由于不同的操作系统（如 Windows、macOS 和 Linux）有不同的二进制包格式和标准，因此有些 Python 包可能只支持特定的操作系统平台。 win32,  macos, win_amd64





--implementation <implementation>

Only use wheels compatible with Python implementation <implementation>, e.g. ‘pp’, ‘jy’, ‘cp’, or ‘ip’. If not specified, then the current interpreter implementation is used. Use ‘py’ to force implementation-agnostic wheels.

`--implementation` 参数通常用于指定要安装的 Python 实现的类型。Python 有多种实现，包括 CPython、Jython、IronPython 和 PyPy 等。CPython 是标准的 Python 实现，也是最常用的。





--abi <abi>

Only use wheels compatible with Python abi <abi>, e.g. ‘pypy_41’. If not specified, then the current interpreter abi tag is used. Use this option multiple times to specify multiple abis supported by the target interpreter. Generally you will need to specify --implementation, --platform, and --python-version when using this option.

这个参数 我看了很久也没有看明白,这个abi 是指什么, 就没有指定. 

以下来自 **文心一言**: 

pip download --abi参数用于指定一个ABI版本。ABI，即Application Binary Interface，是程序二进制接口的缩写。ABI是操作系统为程序二进制接口所定义的一套规范，它规定了程序在运行时所需要的操作系统服务，例如系统调用、寄存器的使用、数据类型的大小等。

在pip download命令中使用--abi参数，可以指定要下载的Python包的ABI版本。这对于在某些特定环境下安装Python包时非常有用，例如当目标系统与源Python环境不兼容时。

请注意，具体的ABI版本取决于特定的Python包，因此需要参考该包的文档或说明来了解其支持的ABI版本。





指定平台信息

```bash
pip download  --only-binary=:all:   --platform win_amd64  --python-version "3.10"  --implementation cp   -d offline_packages -r requirements.txt  --index-url http://mirrors.aliyun.com/pypi/simple/   --trusted-host mirrors.aliyun.com  
```







到指定的机器上进行安装 即可. 

```bash
pip install --no-index --find-links=./offline_packages   -r ./requirements.txt
```

如果发现安装过程中 出现报错, 或者缺少包, 单独下载对应的包,进行单独安装即可. 





### 报错2

离线安装   error no matching distribution found for win32-setctime>1.0.0 

这个我有单独下载了一下 这个包. 到离线的机器上面 执行安装了一下



```bash
pip install  win32_setctime-1.1.0-py3-none-any.whl
```





### 总结

这篇文章说明了离线环境下如何安装下载Python  Package,以及遇到问题该如何解决. 





### 参考文档

https://pip.pypa.io/en/stable/cli/pip_download/

https://pip.pypa.io/en/stable/cli/pip_install/

https://imshuai.com/python-pip-install-package-offline-tensorflow

https://blog.csdn.net/Tohyama/article/details/130646346





<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2023-12-16 18:14:10' --frank 
    </font>
</center>