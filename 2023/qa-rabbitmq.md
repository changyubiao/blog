

# [QA]  rabbitmq 挂载数据 重启  数据丢失问题





## **Question**

在docker-compose.yml 文件中 配置了rabbitmq  服务 并且配置了 valumes ，但是重启service 后 发现 MQ中的消息还是丢了。

`docker-compose.yml` 文件如下

```yaml
version: '3'

services:
  rabbitmq:
    container_name: 'mq'
    image: rabbitmq:management
    restart: always
    ports:
      - "5672:5672"  # AMQP
      - "15672:15672"  # Web UI
    networks:
      - app-network
    environment:
      RABBITMQ_DEFAULT_USER: root
      RABBITMQ_DEFAULT_PASS: xxxxxxxxxxx
      RABBITMQ_DEFAULT_VHOST: /
    volumes:
      - rabbitmq_data:/var/lib/rabbitmq/mnesia


networks:
  app-network:
    driver: bridge

volumes:
  rabbitmq_data:
    driver: local
```





停掉服务后

```bash

docker compose -f docker-compose.yml  down 
```



然后发送一些消息 到MQ里面

重启启动服务：

```
docker compose -f docker-compose.yml  up -d 
```

此时发现数据丢失， MQ 里面的消息全部都不见了





## **Answer**

通过查找 [isssue](https://github.com/docker-library/rabbitmq/issues/106) 发现 需要设定 hostname  才不会丢失持久化的数据. 有一个高赞回复 

```reStructuredText
Rabbitmq uses the hostname as part of the folder name in the mnesia directory. Maybe add a `--hostname some-rabbit` to your docker run?

```



于是 添加 hostname 选项 

```yaml
version: '3'

services:
  rabbitmq:
    hostname: rabbitmq_host
    container_name: 'mq'
    image: rabbitmq:management
    restart: always
    ports:
      - "5772:5672"  # AMQP
      - "15772:15672"  # Web UI
    networks:
      - app-network
    environment:
      RABBITMQ_DEFAULT_USER: root
      RABBITMQ_DEFAULT_PASS: xxxxxxxxxxx
      RABBITMQ_DEFAULT_VHOST: /
    volumes:
      - rabbitmq_data:/var/lib/rabbitmq/mnesia


networks:
  app-network:
    driver: bridge

volumes:
  rabbitmq_data:
    driver: local

```







## 启动命令

重启rabbitmq服务

```
docker compose -f docker-compose.yml  restart rabbitmq
```



停掉所有的服务

```
docker compose  -f docker-compose.yml  down
```





重新拉起来 rabbitmq 服务 

```bash
docker compose  -f docker-compose.yml  up  rabbitmq -d
```







## 参考文档

https://segmentfault.com/q/1010000042249517

https://github.com/docker-library/rabbitmq/issues/106





<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2023-07-19 20:41:12' --frank 
    </font>
</center>