



## split 命令

`split`命令用于将文件分割成较小的部分。它可以按照文件大小、行数或特定的分隔符来划分文件



```bash

[duser@xxxx dir2]$ split --help
Usage: split [OPTION]... [INPUT [PREFIX]]
Output fixed-size pieces of INPUT to PREFIXaa, PREFIXab, ...; default
size is 1000 lines, and default PREFIX is 'x'.  With no INPUT, or when INPUT
is -, read standard input.

Mandatory arguments to long options are mandatory for short options too.
  -a, --suffix-length=N   generate suffixes of length N (default 2)
      --additional-suffix=SUFFIX  append an additional SUFFIX to file names
  -b, --bytes=SIZE        put SIZE bytes per output file
  -C, --line-bytes=SIZE   put at most SIZE bytes of lines per output file
  -d, --numeric-suffixes[=FROM]  use numeric suffixes instead of alphabetic;
                                   FROM changes the start value (default 0)
  -e, --elide-empty-files  do not generate empty output files with '-n'
      --filter=COMMAND    write to shell COMMAND; file name is $FILE
  -l, --lines=NUMBER      put NUMBER lines per output file
  -n, --number=CHUNKS     generate CHUNKS output files; see explanation below
  -u, --unbuffered        immediately copy input to output with '-n r/...'
      --verbose           print a diagnostic just before each
                            output file is opened
      --help     display this help and exit
      --version  output version information and exit

SIZE is an integer and optional unit (example: 10M is 10*1024*1024).  Units
are K, M, G, T, P, E, Z, Y (powers of 1024) or KB, MB, ... (powers of 1000).
```



可以按字节或者按行数进行切割 

```bash
语法：split [OPTION] [``INPUT` `[PREFIX]]
-b： 选项后跟期望切割后的单个文件的大小
-l： 选项后跟期望切割后单个文件的行数，直接写数字
-d： 使用数字作为后缀
-a： 配合选项-d，指定后缀长度(默认值2)
综合语法：split -b byte_count[K|k|M|m|G|g] [-a suffix_length] [file [prefix]]
实际使用：split -b 30M -d -a 2  my_data.tar.gz  my_data.tar.gz.
```





## 示例

```bash
# 按字节 30M 为 一个文件 , 文件后缀是两位, 00, 01 , 02 ... 
split -b 30M -d -a 2  my_data.tar.gz  my_data.tar.gz.

# my_data.tar.gz.00 my_data.tar.gz.01 my_data.tar.gz.02   ,,,,

```





```bash
[xxx@xxx dir2]$ wc -l pangu.sh
1052 pangu.sh

# 按行数进行切割
split -l 100  -d -a 2 pangu.sh   pangu.


[duser@template dir2]$ ll
total 80
-rw-rw-r-- 1 duser duser  3232 2023/09/11 09:11:09 pangu.00
-rw-rw-r-- 1 duser duser  1899 2023/09/11 09:11:09 pangu.01
-rw-rw-r-- 1 duser duser  2070 2023/09/11 09:11:09 pangu.02
-rw-rw-r-- 1 duser duser  2192 2023/09/11 09:11:09 pangu.03
-rw-rw-r-- 1 duser duser  2770 2023/09/11 09:11:09 pangu.04
-rw-rw-r-- 1 duser duser  2569 2023/09/11 09:11:09 pangu.05
-rw-rw-r-- 1 duser duser  2822 2023/09/11 09:11:09 pangu.06
-rw-rw-r-- 1 duser duser  3648 2023/09/11 09:11:09 pangu.07
-rw-rw-r-- 1 duser duser  4537 2023/09/11 09:11:09 pangu.08
-rw-rw-r-- 1 duser duser  3383 2023/09/11 09:11:09 pangu.09
-rw-rw-r-- 1 duser duser   948 2023/09/11 09:11:09 pangu.10
-rw-rw-r-- 1 duser duser 30070 2023/09/11 09:06:54 pangu.sh




# 按字节切割
[xxx@xxx dir2]$ split -b 5k  -d -a 2  pangu.sh  pangu.
[xxx@xxxx dir2]$ ls
pangu.00  pangu.01  pangu.02  pangu.03  pangu.04  pangu.05  pangu.sh 
```







## 还原文件

使用 cat  所有的分割后的文件 ,然后重定向到另一个文件里面即可. 

```bash
[xxx@xxx xxx]$ ls pangu.[0-9]*
pangu.00  pangu.01  pangu.02  pangu.03  pangu.04  pangu.05  pangu.06  pangu.07  pangu.08  pangu.09  pangu.10
[xxx@xxx dir2]$ cat pangu.[0-9]*  > recover.sh
[xxx@xxx dir2]$ ls
pangu.00  pangu.01  pangu.02  pangu.03  pangu.04  pangu.05  pangu.06  pangu.07  pangu.08  pangu.09  pangu.10  pangu.sh  recover.sh

```









<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2023-09-11 19:58:54' --frank 
    </font>
</center>
