# Git 更改remote repo 地址



我们想更换远程 repo 地址, 因为我们仓库更换了名称.

git remote  rename origin 



查看现在的 romote 地址

```bash
# 查看 remote 地址
git remote -v
origin	git@gitee.com:changyubiao/attrdict.git (fetch)
origin	git@gitee.com:changyubiao/attrdict.git (push)
```



```bash
# delete old origin  
git remote remove origin


# add new origin
git remote add origin  git@gitee.com:changyubiao/yydict.git


# pull  code 
git pull origin main --allow-unrelated-histories

# set-upstream 
git push --set-upstream origin main
```





### 参考文档

https://www.cnblogs.com/Nyan-Workflow-FC/p/14454788.html





<center>
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2023-12-10 06:53:23' --frank
    </font>
</center>
