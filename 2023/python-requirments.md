

# python requirements  == 和 ~= 区别

 		

​		我们都知道Python 依赖管理使用  `requirements.txt`  来定义依赖版本的关系，我一般喜欢时候 `==` 固定住某个版本，这样防止项目在第三方依赖更新中，接口不兼容 导致一些潜在的报错，因此我喜欢用 `==`  有的时候 可能不需要那么严谨，我们可能只需要固定住大版本即可，小版本 可能是一些bug 的修复，或者性能优化之类的。 有时候可以使用 `~=` 表示兼容版本，具体详细 的信息 PEP 440 文档有介绍，这里抛砖引玉一下。



在 Python 的 `requirements.txt` 文件中，`==` 和 `~=` 是用来指定依赖包版本的操作符。

1. `==` 操作符：
   - 例如：`package_name==1.2.3`
   - 这表示你要精确地指定依赖包的版本为 `1.2.3`
   - 当运行安装命令时，只会安装与指定版本完全匹配的包
2. `~=` 操作符：
   - 例如：`package_name~=1.2.0`
   - 这表示你希望安装与指定版本兼容的最新修订版本（即第三个数字）。
   - 在这个例子中，你指定了 `1.2.0` 版本，但允许安装任何 `1.2.x` 版本，包括 `1.2.1`、`1.2.5`、`1.2.9` 等
   - 但当下一个主版本号发布时（如 `1.3.0`），就不会自动升级到新的主版本

注意：`==` 和 `~= ` 只是针对固定版本和兼容版本的两种常见情况。你还可以使用其他操作符来指定依赖包的版本要求，如大于等于（`>=`）、小于等于（`<=`）、大于（`>`）、小于（`<`）等。

示例：

- `package_name>=1.2.0`: 表示依赖包的最低版本为 `1.2.0` 或更高版本。
- `package_name<2.0.0`: 表示依赖包的版本要小于 `2.0.0`。





当涉及到Python软件包的依赖管理和版本控制时，以下是一些相关的参考文档和PEP（Python增强提案）：

1. [pip文档](https://pip.pypa.io/en/stable/user_guide/#requirements-files)：pip 是Python的包管理工具，它允许你使用 `requirements.txt` 文件来指定项目的依赖关系。这里的官方文档提供了关于 `requirements.txt` 文件格式和语法的详细信息。
2. [PEP 508 -- Dependency specification for Python Software Packages](https://peps.python.org/pep-0508/)：这个PEP定义了Python软件包的依赖规范，并描述了在 `requirements.txt` 文件中如何指定依赖关系的准确语法。这个PEP对于理解 `==` 和 `~= ` 操作符非常有用。
3. [PEP 440 -- Version Identification and Dependency Specification](https://peps.python.org/pep-0440/)：这个PEP介绍了Python软件包版本号的规范，包括版本号的各个组成部分以及如何比较和匹配版本号。对于理解操作符 `==`、`~= ` 和其他版本比较操作符的行为，这个PEP是一个重要的参考。
4. [compatible-release](https://peps.python.org/pep-0440/#compatible-release)
5.  [Requirement Specifiers](https://pip.pypa.io/en/stable/reference/requirement-specifiers/ )





<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2023-07-23 20:54:16' --frank 
    </font>
</center>

