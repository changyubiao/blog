

# ln命令 – 为文件创建快捷方式



Linux ln（英文全拼：link files）命令是一个非常重要命令，它的功能是为某一个文件在另外一个位置建立一个同步的链接。

当我们需要在不同的目录，用到相同的文件时，我们不需要在每一个需要的目录下都放一个必须相同的文件，我们只要在某个固定的目录，放上该文件，然后在 其它的目录下用ln命令链接（link）它就可以，不必重复的占用磁盘空间。

Linux文件系统中，有所谓的链接(link)，我们可以将其视为**档案的别名**，而链接又可分为两种 : 硬链接(hard link)与软链接(symbolic link)，硬链接的意思是一个档案可以有多个名称，而软链接的方式则是产生一个特殊的档案，该档案的内容是指向另一个档案的位置。硬链接是存在同一个文件系统中，而软链接却可以跨越不同的文件系统。

不论是硬链接或软链接都**不会**将原本的档案复制一份，只会占用非常少量的磁碟空间。





## 软链接 与硬链接 区别

硬链接（hard link）
硬链接实际上是**一个指针**，指向源文件的inode，系统并不为它重新分配inode。硬连接**不会创建新的inode**，硬连接不管有多少个，都指向的是**同一个inode节点**，只是新建一个hard link会把结点连接数增加，只要结点的连接数不是0，文件就一直存在，不管你删除的是源文件还是连接的文件。

只要有一个存在，文件就存在（其实就是**引用计数**的概念)。当你修改源文件或者连接文件任何一个的时候，其他的文件都会做**同步的修改**。



软链接（soft link）
软链接最直观的解释：相当于Windows系统的**快捷方式**，是一个**独立文件**（拥有**独立的inode**,与源文件inode无关），该文件的内容是源文件的路径指针，通过该链接可以访问到源文件。所以删除软链接文件对源文件无影响，但是删除源文件，软链接文件就会找不到要指向的文件（可以类比Windows上快捷方式，你点击快捷方式可以访问某个文件，但是删除快捷方式，对源文件无任何影响）。





**软链接**：

- 1.软链接，以路径的形式存在。类似于Windows操作系统中的快捷方式
- 2.软链接可以 跨文件系统 ，硬链接不可以
- 3.软链接可以对一个不存在的文件名进行链接
- 4.软链接可以对目录进行链接

**硬链接**：

- 1.硬链接，以文件副本的形式存在。但不占用实际空间。
- 2.不允许给目录创建硬链接
- 3.硬链接只有在同一个文件系统中才能创建







`ln` 是一个用于创建链接（link）的命令。它可以创建硬链接和符号链接（也称为软链接或符号连接）。以下是 `ln` 命令的详细解释和用法



符号链接是一个指向文件或目录的特殊文件。它类似于 Windows 操作系统中的快捷方式。符号链接是指向目标文件或目录的路径名，而不是指向实际数据。如果原始文件或目录被删除或移动，符号链接将失效。

**其他常用选项和参数：**

- `-f`：强制执行操作，如果目标文件已经存在，则覆盖它。

- `-i`：在创建硬链接或符号链接之前，提示用户是否覆盖现有文件。

- `-n`：禁止解引用符号链接，仅在符号链接上操作，而不是其指向的文件。

- `-v`：显示详细输出，包括创建的链接的名称。

  

```bash
ln -s [OPTIONS] FILE LINK
```



注意 ln 参数 最后一个表示 新创建的软连接

```bash
ln -s source_file   symbolic_link
```



```
ln -s myfile.txt   myfile.link

 ~/2222  ll
total 0
lrwxr-xr-x@ 1 frank  staff    10B  9 25 09:50 myfile.link -> myfile.txt
-rw-r--r--@ 1 frank  staff     0B  9 25 09:49 myfile.txt
```





```bash
 echo "Hello world" > myfile.txt
 ls
myfile.link myfile.txt
 cat myfile.link
Hello world
 cat myfile.txt
Hello world
```





删除软链接 

```bash
> unlink myfile.link

# 方式二  rm 
> rm myfile.link
```









https://www.linuxcool.com/ln

https://www.runoob.com/linux/linux-comm-ln.html

Linux 命令大全 https://www.runoob.com/linux/linux-command-manual.html

Ln Command in Linux https://linuxize.com/post/how-to-create-symbolic-links-in-linux-using-the-ln-command/

