



# tar 命令



tar 命令一般用来打包文件 ,文件夹 , 方便传输使用. `tar`命令是在Linux和UNIX系统上用于创建、查看和提取tar归档文件的工具。它通常与gzip一起使用，以便在创建归档文件时进行压缩或解压缩。



- `-c`: 创建归档文件

- `-x`: 提取文件

- `-z`: 告诉 `tar` 命令使用 gzip 解压缩文件。

- `-f`: 指定要解压缩的文件名

- `-t`: 列出归档文件中的内容

  



```bash 
# 打包命令
# tar  zcvf <打包文件名称.tar.gz>  dir file dir2 ...
该命令将创建名为`demo.tar.gz`的归档文件，并将指定的文件和目录添加到归档中。
tar  zcvf demo.tar.gz   demo
```





```bash
# 解包命令
tar  zxvf demo.tar.gz   demo
```





```bash
# 查看包内容,不解压
tar tzf  demo.tar.gz  
```





也可以打包多个文件, 文件夹 

```bash
# 此时会创建一个 demo.tar.gz 的归档文件, 文件内容有 dir1 ,dir2 , file.txt 三个文件
tar zcvf  demo.tar.gz  dir1  dir2  file.txt 
```



解压指定到一个目录

将tar文件解压到指定目录，您可以使用 `-C` 参数来指定目标目录

```bash
tar -xf 文件名.tar -C 目标目录
```



```bash
# 创建文件夹
mkdir 1111
# 解压到指定目录
tar zxvf  demo.tar.gz   -C  1111/
```













<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2023-09-10 11:35:34' --frank 
    </font>
</center>



