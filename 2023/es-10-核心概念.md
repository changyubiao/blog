



Elasticsearch 核心概念



分布式的搜索,存储,分析引擎

搜索类数据库

ES 

应用广泛  

   全文搜索引擎, 垂直类搜索

​	ELK 







## 节点  

每一个节点 就是 elasticsearch 就是Java的进程, 每一个节点就是elasticsearch 的实例

一个节点 不等于一台服务器



节点角色

master  候选节点

data  数据节点



![image-20230811112625510](./image/es-10-核心概念/image-20230811112625510.png)

![image-20230811112644765](./image/es-10-核心概念/image-20230811112644765.png)

node3 是主节点, node-1,node-2 候选节点





## 分片是一个lucene 实例

一个索引包含一个或者 多个分片

主分片的数量不能修改,一旦创建不能修改, 而副本可以动态调整,

每一个分片都是 一个Lucene 实例, 有完整的创建索引 和处理请求的能力.



一个 doc **不能**同时存在多个主分片中. 但是 每个主分片的副本数量大于1 时候, 可以同时存在到多个副本中.

每个主分片 和其副分片 不能同时在同一个节点上. 所以最低可用配置是两个节点互为主备

ES 会自动在nodes 上做分片均衡 , shard  rebanlance 





分片是 Elasticsearch 集群分发数据的单元



![image-20230811113800419](/Users/frank/2023/typora_docs/August/image/es-10-核心概念/image-20230811113800419.png)





## 集群

原生分布式

一个节点  不等于 一台服务器 



集群状态

健康值状态



Green  所有的primary 和replica 均为 active ,  所有主分片和从分片都准备就绪（分配成功

Yelllow  至少 一个Replica 不可用 , 所有主分片准备就绪，但存在至少一个主分片（假设是 A）对应的从分片没有就绪，此时集群属于警告状态

Red  至少有一个主分片没有就绪,数据不完整,集群不可用



健康 检查 

```bash
GET  _cat/health

GET  _cat/health?v


GET  _cluster/health 
```





Quesiton: 

1. 分片 与节点的关系是什么?     

   一个节点 上可以有多个分片,每个节点对应一个Java 进程.  注意是数据节点 才能有多个分片