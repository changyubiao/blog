



# 从命令行里打开pycharm 项目

在MacBook 笔记本下面,想通过 `pycharm + 路径` 这种方式打开项目,  这样是不是很方便呢. 于是找了一下, 还真的可以. 只要编写一个简单的脚步即可实现. 



编写脚本 `pycharm` 把脚本路径放到 Path环境变量中. 

```bash
#!/bin/sh
# 脚本名称: pycharm
# 通过命令行 快速打开项目
open -na "PyCharm.app" --args "$@"
```

设置权限

```bash
chmod +x  pycharm 
```

把这个脚本添加到Path 路径下. 



这个命令是用于在 macOS 操作系统上打开 PyCharm 应用程序的。下面我会逐一解释这个命令的各个部分：

1. `open`: 这是一个 macOS 命令行工具，用于打开文件、目录或应用程序。
2. `-na`: 这是 `open` 命令的两个选项。
   - `-n`: 这个选项表示在新进程中打开应用程序。
   - `-a`: 这个选项后面通常会跟着一个应用程序的名称，用于指定要打开的应用程序。
3. `"PyCharm.app"`: 这是 PyCharm 应用程序的名称。注意，这里使用了双引号，是因为应用程序名称中包含一个点（.），在 shell 脚本中，点（.）通常是一个特殊字符，用于表示当前目录。使用双引号可以确保 shell 将点（.）视为普通字符，而不是特殊字符。
4. `--args "$@"`: 这部分将传递给 PyCharm 应用程序的所有参数都传递给它。`"$@"` 是 shell 脚本中的一个特殊变量，表示所有传递给脚本或函数的参数,每个参数都被视为独立的字符串，即使它们包含空格或其他特殊字符。

综上所述，这个命令的作用是在新进程中打开 PyCharm 应用程序，并将所有传递给这个命令的参数都传递给 PyCharm 应用程序。



打开 `iTerm` 进入到 项目目录 

```bash
cd /Users/frank/proj/simpledict

pycharm $(pwd)
```









### 参考文档

https://www.jetbrains.com/help/pycharm/working-with-the-ide-features-from-command-line.html#standalone







<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2023-12-24 09:50:35' --frank 
    </font>
</center>
