

## Jenkins插件-Timestamper插件的使用 



### 背景描述

有时候 我们希望 在构建程序的过程中，输出的信息打印当前时间。

对于Pipeline项目的项目 

```groovy
pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                echo 'Hello World'
                script {
                    sleep(time: 5, unit: 'SECONDS')
                }
                echo 'Build Done'
            }
        }
    }
}
```







![image-20250308090934822](./image/Jenkins插件-Timestamper的使用/image-20250308090934822.png)

发现 我看不到任务执行的时间。





### 安装Timestamper 插件

使用 Timestamper 插件 ， 首先 搜索安装 这个插件 



搜索安装 插件  Dashboard > 系统管理 > 插件管理 



![image-20250308090250919](./image/Jenkins插件-Timestamper的使用/image-20250308090250919.png)





在Jenkins中配置Timestamper插件以显示时间戳的过程相对直接。首先，确保你已经安装了Timestamper插件。如果你还没有安装它，可以通过Jenkins的插件管理页面进行安装。



Timestamper插件安装完成，你可以按照以下步骤来配置你的Pipeline或自由风格项目（Freestyle project）以在控制台输出中显示时间戳：

### 对于Pipeline项目

对于使用Declarative Pipeline或Scripted Pipeline的项目，你可以通过在流水线脚本中添加`timestamps`指令来启用时间戳。根据最新的更新信息，如果你想要为所有的Pipeline构建启用时间戳，无需在每个Pipeline脚本中手动包裹`timestamps{}`代码块。相反，你可以通过系统设置来全局启用时间戳功能。具体操作如下：

1. 登录到Jenkins。
2. 进入“Manage Jenkins”（管理Jenkins）。
3. 选择“Configure System”（系统配置）。
4. 找到Timestamper插件的配置部分，并勾选 "Enabled for all Pipeline builds" 选项。
5. 如果需要自定义时间戳格式，可以在相应的文本框中输入你想要的时间格式。



![image-20250308091326155](./image/Jenkins插件-Timestamper的使用/image-20250308091326155.png)



但是，如果你想只为特定的Pipeline启用时间戳，或者你正在使用的是较早版本的Timestamper插件，你需要在Pipeline脚本中明确地使用`timestamps`指令。例如，在一个Declarative Pipeline中，你可以这样做：

```
pipeline {
    agent any
    options {
        timestamps()
    }
    stages {
        stage('Example') {
            steps {
                echo 'Hello, World!'
                script {
                    sleep(time: 5, unit: 'SECONDS')
                }
                echo 'Build Done'
            }
        }
    }
}

```



如果使用 比较新的版本 不需要写  timestamps()

```groovy
pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                echo 'Hello World'
                script {
                    sleep(time: 5, unit: 'SECONDS')
                }
                echo 'Build Done'
            }
        }
    }
}
```

对于Scripted Pipeline，你可以这样写：

```groovy
timestamps {
    node {
        stage('Example') {
            echo 'Hello, World!'
        }
    }
}
```

### 对于自由风格项目（Freestyle project）

如果你使用的是传统的自由风格项目，那么启用时间戳的方式略有不同。你需要进入项目的配置页面，并找到“Build Environment”（构建环境）部分。在那里，你会看到一个名为“Add timestamps to the Console Output”的复选框。勾选这个选项即可为该项目的所有构建输出添加时间戳。

#### 时间戳格式化

如果默认的时间戳格式不符合你的需求，你可以通过修改Timestamper插件的全局配置来调整时间戳的格式。这通常包括日期和时间的格式化字符串，允许你指定如何显示时间和日期。例如，你可以设置时间戳显示为`yyyy-MM-dd HH:mm:ss`这样的格式。

#### 注意事项

- 确保Timestamper插件已正确安装并处于活动状态。
- 根据你的Jenkins版本和Timestamper插件版本的不同，配置方式可能会有所不同，请参考具体的文档或帮助信息。
- 在某些情况下，可能需要重启Jenkins才能使更改生效。





### 时间显示的配置 

这个插件 可以自定义时间显示的形式。



在Jenkins中配置Timestamper插件的时间格式时，你可以根据需要选择不同的时间格式。你提到的两个选项：

- `'<b>'HH:mm:ss'</b> '`：这种格式会显示小时、分钟和秒，但不包括毫秒。
- `'<b>'HH:mm:ss.S'</b> '`：这种格式不仅包含小时、分钟和秒，还会显示秒后的第一个小数位（即十分之一秒）。

为了配置这些格式，你需要访问Jenkins的系统配置页面，并找到Timestamper插件的相关设置。以下是具体步骤：

1. 登录到你的Jenkins实例。
2. 点击左侧菜单中的“Manage Jenkins”（管理Jenkins）。
3. 在“管理Jenkins”页面中，找到并点击“Configure System”（系统配置）链接。
4. 向下滚动到Timestamper插件的部分。在这里，你应该能看到一个用于设置时间戳格式的文本框。
5. 在该文本框中输入你想要使用的时间格式字符串。例如，如果你想要采用`'<b>'HH:mm:ss'</b> '`格式，则直接输入这个字符串；若要使用带毫秒的格式，则输入`'<b>'HH:mm:ss.S'</b> '`。

请注意，这里的`HH:mm:ss`遵循Java的`SimpleDateFormat`模式，其中：
- `HH` 表示24小时制的小时数（00-23）。
- `mm` 表示分钟数（00-59）。
- `ss` 表示秒数（00-59）。
- `S` 代表毫秒。

因此，如果你想让时间戳精确到十分之一秒，可以使用`.S`；如果需要更精确的时间，比如三位毫秒值，可以使用`.SSS`。

完成上述设置后，记得保存更改。这样，所有新生成的控制台输出都将应用新的时间戳格式。







再次 重新 构建 Pipeline 的项目 

```groovy
pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                echo 'Hello World'
                script {
                    sleep(time: 5, unit: 'SECONDS')
                }
                echo 'Build Done'
            }
        }
    }
}

```





![image-20250308091903660](./image/Jenkins插件-Timestamper的使用/image-20250308091903660.png)







### 参考文档

SimpleDateFormat 时间显示形式   https://docs.oracle.com/javase/8/docs/api/java/text/SimpleDateFormat.html

Timestamper 配置方式   https://plugins.jenkins.io/timestamper/
