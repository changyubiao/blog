

## Jenkins QA 遇到的问题以及解决方案







#### Q1:  插件警告：  Pipeline: Declarative 2.2205.vc9522a_9d5711

当前安装的下列组件已有警告发布。 Pipeline: Declarative 2.2205.vc9522a_9d5711 Restarting a run with revoked script approval allowed A fix for this issue is available. Go to the plugin manager to update the plugin.



​	Dashboard > 系统管理 > 插件管理    

[插件管理](http://jenkins.zhiexa.com:8900/manage/pluginManager/)



更新对应插件即可





#### Q2：  Jenkins 根 URL 配置

问题描述： 	Jenkins 的根 URL 为空，但对于 Jenkins 的很多特色是必需的，例如：邮件通知， PR 状态更新，以及像 `BUILD_URL` 一样的环境变量。

请在 [Jenkins 配置](http://jenkins.zhiexa.com:8900/configure) 提供一个准确的值。

```
http://jenkins.zhiexa.com:8900/
```





#### Q3 :  Jenkins 构建完成后 没有构建视图 

阶段视图

缺少插件：  到插件管理的位置  Dashboard > 系统管理 > 插件管理 ,  搜索： pipeline Stage View   安装 插件即可，

安装完成后，重新 Jenkins 服务 即可，可以看到阶段视图



![image-20250308125158882](./image/Jenkins QA以及解决方案/image-20250308125158882.png)









