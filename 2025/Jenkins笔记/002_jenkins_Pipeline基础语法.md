[TOC]


## Jenkins 核心概念 
今天我们来学习Jenkins Pipeline的基本语法。为我们以后编写 Pipeline 脚本提供一些基础的经验。


### Pipeline组成部分 
Jenkins Pipeline（或简称为 "Pipeline"）是一套插件，将持续交付的实现和实施集成到 Jenkins 中。

持续交付 Pipeline 自动化的表达了这样一种流程：将基于版本控制管理的软件持续的交付到您的用户和消费者手中。

Jenkins Pipeline 提供了一套可扩展的工具，用于将“简单到复杂”的交付流程实现为“持续交付即代码”。Jenkins Pipeline 的定义通常被写入到一个文本文件（称为 `Jenkinsfile` ）中，该文件可以被放入项目的源代码控制库中。



```groovy
pipeline {
    agent any

    environment {
        // 定义全局环境变量
        DOCKER_REGISTRY = '172.19.89.106:12300'
        IMAGE_NAME = 'saas-cloud-job-service'
        SHORT_NAME = 'cloud-job'
        GIT_URL = 'http://xxxxxxxxxxxxxx/xxxxxxxxx/xxxxxxxxxx/xxxxxxx.git'
        CREDENTIALS_ID = '080912b9-902a-49f1-89e1-4684586ab238'
    }

    parameters {
        choice(name: 'BRANCH', choices: ['cicd','dev', 'test', 'master'], description: 'Select the branch to build')
        choice(name: 'ENV', choices: ['test', 'prod'], description: 'Select the environment to deploy')
    }
    stages {
        stage('Pull code') {
            steps {
                script {
                    // 使用参数化的分支名称进行 checkout
                    def branchName = params.BRANCH
                    checkout scmGit(
                        branches: [[name: "*/${branchName}"]],
                        extensions: [],
                        userRemoteConfigs: [[credentialsId: CREDENTIALS_ID, url: GIT_URL]]
                    )
                }
            }
        }

        stage('Build project') {
            steps {
                script {
                    def branchName = params.BRANCH
                    env.branchName = branchName  // 设置环境变量

                    // 获取 Git SHA 和当前日期
                    def gitSha = sh(returnStdout: true, script: 'git rev-parse --short HEAD').trim()
                    def currentDate = sh(returnStdout: true, script: 'date +%Y%m%d').trim()

                    // 构建 Docker 镜像标签
                    env.IMAGE_TAG = "${DOCKER_REGISTRY}/${IMAGE_NAME}/${SHORT_NAME}:${branchName}-${currentDate}-${gitSha}"

                    // 打印信息
                    // echo "Building Docker image with tag: ${env.IMAGE_TAG}"

                    // 构建 Docker 镜像
                    sh """
                        echo " ============================= 开始构建docker image ============================= "
                        docker build -f Dockerfile . -t ${env.IMAGE_TAG}
                        echo " ============================= docker image 构建完成  ============================= "
                        echo "[TAG]: ${env.IMAGE_TAG}"
                    """
                }
            }
        }

        stage('Push Docker image') {
            steps {
                script {
                    // 登录并推送 Docker 镜像
                    dockerLoginAndPush(env.IMAGE_TAG)
                }
            }
        }

        stage('Deploy project') {
            steps {
                echo 'Publishing project'
                // 这里可以添加部署的具体步骤
            }
        }
    }

    post {
        always {
            echo 'This pipeline will always run, begin cleaning workspace.'
            cleanWs() 
        }
        success {
            echo 'The pipeline succeeded.'
        }
        failure {
            echo 'The pipeline failed.'
        }
        changed {
            echo 'The pipeline status has changed.'
        }
    }
}


def dockerLoginAndPush(String imageTag) {
    /**
    使用 withDockerRegistry 插件来简化 Docker 登录和推送
    */ 
    withDockerRegistry([credentialsId: 'docker-harbor-registry-credentials', url: "http://${env.DOCKER_REGISTRY}"]) {
        try {
            // 推送 Docker 镜像
            sh "docker push ${imageTag}"
            echo "Pushed Docker Image [TAG]: ${imageTag}"
        } catch (err) {
            error "Failed to push Docker image: ${err}"
        }
    }

}
```




这个 Jenkins Pipeline  是有几个部分 agent, environment, stages,post , 定义了这几个部分。


`agent` 块：  定义了这个脚本需要在哪里跑，`any` 表示任何一个节点 


`environment` 块 ： 在`environment`块中定义了全局环境变量，这些变量在整个Pipeline过程中都可访问。例如，`DOCKER_REGISTRY`是你的私有Docker仓库地址，`IMAGE_NAME`是你想要构建的Docker镜像名称等。



`parameters` 块： 通过`parameters`块允许用户在启动Pipeline时选择参数值，这里有两个下拉选项：

- `BRANCH`：选择要构建的Git分支。
- `ENV`：选择部署环境（测试或生产）。



`stages` 块： 这里定义 整个发布程序的阶段， 一般 我们会把发布项目定义几个不同的阶段，比如 ：拉取代码、构建项目、推送Docker镜像和部署项目。当然这里可以根据自己的需要 自行定义阶段 。 



`post`块定义了一系列动作，它们将在整个Pipeline完成之后运行，无论成功还是失败。包括：

- `always`：总是执行的动作，如清理工作空间。
- `success`/`failure`：根据Pipeline最终状态执行不同的动作。
- `changed`：当Pipeline的状态发生变化时触发（例如从失败变为成功）。

post 块中一般定义一些操作，比如执行失败了发邮件， 或者 Pipeline 执行完成后， 执行一些 空间清理工作。 或者一些状态改变做一些动作等。



`dockerLoginAndPush`  定义了一个辅助函数`dockerLoginAndPush`，它封装了登录Docker仓库和推送镜像的过程，简化了Pipeline脚本的编写。 抽出来一个函数 把 推送镜像的封装成一个函数，方便复用。 



大家大概对这几个部分有个基本的概念即可，后面会慢慢熟悉每一部分的功能，以及每一步的作用。





### 参考文档

[流水线定义参考]( https://www.jenkins.io/zh/doc/book/pipeline/) 





<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2025-03-16 21:38:12' --frank 
    </font>
</center>
