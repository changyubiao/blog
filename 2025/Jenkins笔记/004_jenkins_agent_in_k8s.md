

## Jenkins 动态代理与Kubernetes集成概述



Jenkins 的动态代理（Dynamic Agents）主要用于实现按需创建和销毁构建节点的能力，尤其在 Kubernetes（K8S）环境中，其核心作用是优化资源利用与提升 CI/CD 效率。以下是具体分析：

### 一、动态代理的核心作用

1. **按需扩缩容**
   Jenkins 根据构建任务的需求动态创建临时代理节点（Slave Pod），任务完成后自动销毁节点，避免空闲资源占用。例如，当触发流水线时，K8S 会自动生成一个 Pod 作为构建环境，任务结束后释放资源。
2. **资源隔离与弹性**
   每个任务运行在独立的 Pod 中，避免环境冲突（如依赖版本差异），同时利用 K8S 的调度能力实现负载均衡。
3. **环境一致性**
   通过容器化技术，动态代理基于预定义的 Pod 模板生成节点，确保所有构建环境（如 JDK、Maven 版本）完全一致。
4. **降低运维成本**
   无需手动维护长期运行的物理节点或虚拟机，所有代理节点的生命周期由 Jenkins 与 K8S 自动管理。

------

### 二、动态代理与 K8S 的关系

1. **动态代理的实现依赖 K8S**
   Jenkins 动态代理的典型场景是将节点部署在 K8S 集群中。通过安装 **Kubernetes Plugin**，Jenkins Master 能够与 K8S API 交互，按需创建 Slave Pod 作为临时构建节点。
2. **具体部署方式**
   - **Pod 模板定义**：在 Jenkins 中配置 Pod 模板（指定容器镜像、资源限制、存储卷等），例如为 Java 项目定义包含 JDK 和 Maven 的容器。
   - **任务触发机制**：当流水线任务启动时，Jenkins 调用 K8S API 创建 Pod，任务结束后自动销毁 Pod。
   - **资源持久化**：通过 K8S 的持久化存储（如 PV/PVC）保留 Jenkins 主节点数据（如配置、插件），而动态代理节点无需持久化。

------

### 三、传统静态节点 vs. 动态代理

| **对比项**     | **静态节点**             | **动态代理（K8S）**            |
| -------------- | ------------------------ | ------------------------------ |
| **资源占用**   | 长期运行，资源浪费       | 按需创建，任务结束即释放       |
| **环境一致性** | 依赖手动维护，易出现差异 | 通过容器镜像标准化环境         |
| **扩展性**     | 需手动添加物理节点       | 自动扩缩容，适应高并发构建     |
| **适用场景**   | 固定环境的小规模团队     | 云原生、大规模或复杂依赖的项目 |

------

### 四、典型应用场景

- **微服务多环境构建**：为不同服务生成独立的构建环境，避免依赖冲突。
- **混合云部署**：通过 K8S 跨集群调度，在多个云平台动态创建代理节点。
- **资源敏感型任务**：例如大数据处理任务，仅在需要时申请高性能 Pod。



Jenkins创建动态代理的好处主要体现在以下几个方面：

- **提高资源利用率**：通过动态创建和销毁slave-pod，避免资源浪费。
- **增强构建过程的灵活性**：允许在运行时根据需求选择合适的节点执行任务。
- **简化代码维护**：通过代理模式分离增强功能代码与业务代码，降低耦合度，提升可维护性。

------

### 五、总结一下

Jenkins 动态代理的核心是通过 K8S 实现构建节点的自动化生命周期管理，适用于云原生环境下的高效 CI/CD 流程。

Jenkins与Kubernetes的集成可以实现自动化构建和CI/CD流程。通过在Kubernetes集群上部署Jenkins，并利用Jenkins的Kubernetes插件，可以动态创建和管理代理节点（slave），从而提高构建效率和资源的利用率。







### 六、 如何实现动态代理？

首先前置条件，已经有了 K8S 集群，已经有Jenkins ， 我们希望通过动态代理技术，把agent节点部署到K8S 集群中，生成临时的`pod` ，作为发布机器节点来构建镜像，发布等操作。



首先要安装 `Kubernetes插件` 和  `Kubernetes CLI Plugin插件`

![image-20250317072714336](./image/004_jenkins_agent_in_k8s/image-20250317072714336.png)



#### 配置 Kubernetes 插件



#### 添加 Kubernetes 集群的连接凭据

在 Jenkins 中添加一个连接到 Kubernetes 集群的凭据：

1. 进入“Manage Jenkins”。
2. 点击“Manage Credentials”。
3. 选择“Global credentials (unrestricted)”。
4. 点击“Add Credentials”，凭据类型选择 `Secret file`



把k8s中连接凭据 `$HOME/.kube/config`  复制到本机的一个位置 ，这个文件用来创建连接凭据



![image-20250317082532154](./image/004_jenkins_agent_in_k8s/image-20250317082532154.png)



这样就把凭据 创建好了

![image-20250317082635154](./image/004_jenkins_agent_in_k8s/image-20250317082635154.png)









#### 配置 Jenkins 系统设置中的 Kubernetes 云



![image-20250317081112092](./image/004_jenkins_agent_in_k8s/image-20250317081112092.png)





![image-20250317081248560](./image/004_jenkins_agent_in_k8s/image-20250317081248560.png)



API Server 端点地址

![image-20250317080948760](./image/004_jenkins_agent_in_k8s/image-20250317080948760.png)



![image-20250317081503645](./image/004_jenkins_agent_in_k8s/image-20250317081503645.png)





现在我们可以来测试一下， 我们可以通过下面的Pipeline 来测试 

```groovy
pipeline {
    agent {
        kubernetes {
            // 定义一个多容器 Pod
            yaml '''
apiVersion: v1
kind: Pod
metadata:
  labels:
    app: jenkins-slave
  namespace: dev
  name: jnlp

spec:
  initContainers:
  - name: setup-ssh
    image: 172.19.89.106:12300/library/busybox:v1.37.0
    imagePullPolicy: IfNotPresent
    command: ['sh', '-c', 'cp /tmp/ssh-secret/* /home/jenkins/.ssh/ && chown -R 1000:1000 /home/jenkins/.ssh && chmod 700 /home/jenkins/.ssh && chmod 600 /home/jenkins/.ssh/id_rsa']
    volumeMounts:
    - mountPath: "/tmp/ssh-secret"
      name: ssh-key
      readOnly: true
    - mountPath: "/home/jenkins/.ssh"
      name: ssh-volume
    env:
    - name: JENKINS_AGENT_TIMEOUT
      value: "3600"  # 60 分钟
    - name: JENKINS_AGENT_READ_TIMEOUT
      value: "3600"  # 60 分钟

  containers:
  - name: jnlp
    image: 172.19.89.106:12300/library/jenkins/inbound-agent:alpine
    imagePullPolicy: IfNotPresent
    securityContext:
      runAsUser: 1000   # 确保以 jenkins 用户的身份运行
    resources:
      requests:
        memory: "512Mi"  # 200M
        cpu: "200m"      # 512m核心
      limits:
        memory: "2048Mi"
        cpu: "2000m"
    volumeMounts:
    - mountPath: "/home/jenkins/.ssh"
      name: ssh-volume
    - mountPath: "/home/jenkins/agent"
      name: workspace-volume

  volumes:
  - name: ssh-key
    secret:
      secretName: my-ssh-key-secret
      items:
      - key: id_rsa
        path: id_rsa
        mode: 0600
  - name: ssh-volume
    emptyDir: {}
  - name: workspace-volume
    emptyDir: {}
'''
        }
    }

    options {
        // 设置整个 Pipeline 的超时时间为 80 分钟
        timeout(time: 80, unit: 'MINUTES')
    }

    environment {
        // 定义环境变量
        BRANCH = "${params.BRANCH}"
        IMAGE_NAME = 'saas-cloud-document-draft-service'
        SHORT_NAME = 'draft'

    }

    stages {
        stage('Test') {
            steps {
                script {
                    // 打印工作目录
                    echo "Current working directory: ${pwd()}"
                    // 打印环境变量
                    echo "Environment variables:"
                    env.each { key, value ->
                        echo "${key}: ${value}"
                    }
                    sleep(time:10,unit:'MINUTES')
                }
            }
        }
    } 

}
```



可以到官方jenkins 找到这个镜像， 我这里使用私有仓库

Jnlp 这个容器： 

image : `172.19.89.106:12300/library/jenkins/inbound-agent:alpine`

initContainers 中 setup-ssh 配置一些权限使用的初始化容器。



`stages` 块中 我定义了一个 `Test  Stage`, 这里啥也不干，就是打印变量，以及sleep 一下，方便我们来查看agent 是否 成功创建。



把上面的脚本放到 Pipeline Script中。创建完成后，我们点击立即构建。



![image-20250317084728422](./image/004_jenkins_agent_in_k8s/image-20250317084728422.png)

![image-20250317084628794](./image/004_jenkins_agent_in_k8s/image-20250317084628794.png)







我们在`kuboard` 中 查看 已经成功创建 `pod` ,我们可以进一步查看容器 jnlp 的日志

![image-20250317074414331](./image/004_jenkins_agent_in_k8s/image-20250317074414331.png)



点击 追踪日志 可以看到已经 成功连接上了

![image-20250317074532368](./image/004_jenkins_agent_in_k8s/image-20250317074532368.png)

我们看到这里说明 agent 已经可以成功创建出来了。  这样 Jenkins 动态代理就配置完成了，下一节 我们就可以把构建的所有步骤都放在 动态代理里面来操作了，而不用在真实的物理机上面构建，而是直接在 K8S 创建的pod 中来构建，发布流程。下一篇文章我们就开始实战篇了，敬请期待。





### 参考文档

Kubernetes CLI 插件  https://plugins.jenkins.io/kubernetes-cli/

Kubernetes插件  https://plugins.jenkins.io/kubernetes/

https://blog.csdn.net/m0_50589374/article/details/125950093







<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2025-03-17 08:51:40' --frank 
    </font>
</center>
