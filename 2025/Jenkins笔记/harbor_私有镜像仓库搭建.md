

## Harbor私有仓库镜像搭建

Harbor 用来存储 私有的镜像 ，项目私有镜像仓库，服务器上搭建私有镜像仓库。



#### Step_1-前置工作-安装Docker (如已安装, 则跳过)

环境信息： Operating System--Linux (Ubuntu 24.04)

前置条件， 有 `docker`, `docker compose` 这些基础的环境，如果没有要自己安装。



##### 1.1--阿里云版 (Ubuntu 24.04)

```bash
# 1.1.1--更新软件包列表
sudo apt update

# 1.1.2--安装依赖包
sudo apt-get -y install ca-certificates curl

# 1.1.3--创建/etc/apt/keyrings目录
cd /etc/apt
mkdir keyrings

# 1.1.4--下载Docker的官方GPG密钥
sudo install -m 0755 -d /etc/apt/keyrings
sudo curl -fsSL http://mirrors.cloud.aliyuncs.com/docker-ce/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
sudo chmod a+r /etc/apt/keyrings/docker.asc

# 1.1.5--将Docker仓库添加到系统的软件源列表
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] http://mirrors.cloud.aliyuncs.com/docker-ce/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# 1.1.6--更新软件包列表
sudo apt update

# 1.1.7--安装Docker
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

# 1.1.8--查看版本号
docker -v
```

##### 1.2--完整版 (服务器上有yum, 再使用)

https://blog.csdn.net/liu_chen_yang/article/details/123842609

##### 1.3--简易版 (服务器上有yum, 再使用)

```bash
# 安装yum-config-manager配置工具
yum -y install yum-utils

# 建议使用阿里云yum源: (推荐)
# yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo

# 安装docker-ce版本
yum install -y docker-ce

# 启动
systemctl enable --now docker
```

##### 1.4--Docker常用命令

```bash
# 查看版本号
docker --version

# 查看所有容器 (包括停止的)
docker ps -a

# 查看系统信息
docker info

# 查看镜像列表
docker images

# 删除镜像
docker rmi (IMAGE ID 或 REPOSITORY:TAG)

# 查看卷列表
docker volume ls
```

##### 1.5--启动Docker和设置开机自启

```bash
# 启动Docker
sudo systemctl start docker

# 停止Docker
sudo systemctl stop docker

# 重新启动Docker
sudo systemctl restart docker

# 重新加载 service 配置文件
sudo systemctl daemon-reload

# 设置开机自启
sudo systemctl enable docker

# 查看Docker状态
sudo systemctl status docker
```


#### Step_2-前置工作-更换Docker镜像源 (如已更换, 则跳过)

```bash
# 2.1--移动到指定目录
cd /etc/docker/

# 2.2--新建配置文件 (如有同名文件, 重命名为daemon.json.backup)
vim daemon.json

# 2.3--添加镜像源
{
  "registry-mirrors": [
        "https://dockerproxy.com",
        "https://docker.m.daocloud.io",
        "https://dh-mirror.gitverse.ru",
        "https://jockerhub.com",
        "https://njrds9qc.mirror.aliyuncs.com"
  ]
}

# 2.4--重启Docker
sudo systemctl stop docker
sudo systemctl start docker

# 2.5--拉取镜像  测试 
docker pull hello-world
```


#### Step_3-前置工作-安装Docker Compose (如已安装, 则跳过)

##### 3.1--阿里云版

```bash
sudo apt install docker-compose
```

##### 3.2--完整版

https://blog.csdn.net/liu_chen_yang/article/details/124688952

##### 3.3--简易版

```bash
curl -SL https://github.com/docker/compose/releases/download/v2.16.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose

chmod +x /usr/local/bin/docker-compose
```

##### 3.4--查看版本号

```bash
docker-compose --version
```

#### Step_4--下载离线安装包 (自行选择版本) 

这里采用离线安装 

##### 4.1--查看版本号

https://github.com/goharbor/harbor/releases

##### 4.2--下载地址

https://github.com/goharbor/harbor/releases/tag/v2.10.3

##### 4.3--注意事项

```bash
# 4.3.1
harbor-offline开头和tgz结尾 !!!
(harbor-offline-installer-v2.10.3.tgz)
```


#### Step_5--使用离线安装包

##### 5.1--远程传输

使用文件传输工具(Transmit等), 传输至远程服务器.

##### 5.2--解压安装包

```bash
# 5.2.1
tar -xvf harbor-offline-installer-v2.10.3.tgz
(远程传输后所在的目录, cd /home/apps/)
```

##### 5.3--需更改的配置信息

```bash
# 5.3.1
Hostname改为本机IP或者本机域名

# 5.3.2
把Http的端口改为自己想要映射的端口

# 5.3.3
Https相关信息, 进行注释.
```

##### 5.4--修改前的准备工作

```bash
# 5.4.1--进入harbor目录
cd /home/apps/harbor

# 5.4.2--配置文件复制和改名
cp -ar harbor.yml.tmpl harbor.yml

# 5.4.3--vim模式处理
vim harbor.yml
```

##### 5.5--修改配置文件 

```bash
# 5.5.1--hostname (更改为本机IP 或 域名)
hostname: 47.xxx.xxx.137

# 5.5.2--port (自定义端口)
http:
  port: 12300

# 5.5.3--harbor_admin_password (更改admin用户的密码)
harbor_admin_password: admin123456

# 5.5.4--data_volume (配置数据仓库)
data_volume: /data/harbor

# 5.5.5--https部分的信息, 手动进行注释.

# 5.5.6--保存并退出
```


#### Step_6--开始安装 (harbor目录下操作)

```bash
# 6.1--Harbor安装环境预处理
./prepare

# 6.2--安装并启动Harbor
./install.sh

# 6.3--检查是否安装成功
docker-compose ps

# 6.4--再次安装
# docker-compose up -d
# docker-compose up -f docker-compose.yml -d

# 6.5--停止服务
# docker-compose down
```


#### Step_7--修改Docker配置信息

```bash
# 7.1--配置访问地址
vim /etc/docker/daemon.json
# 添加如下内容 (客户端访问的网址)
   "registry-mirrors": [
            # 已存在, 则无需添加.
            "https://njrds9qc.mirror.aliyuncs.com"
         ],
   "insecure-registries": ["47.xxx.xxx.137:12300"]

# 7.2--重启docker和harbor容器 (harbor目录下操作)
systemctl restart docker
docker-compose stop
docker-compose down
docker-compose up -d

# 7.3--docker登录方式
# docker login 47.xxx.xxx.137:12300
docker login -uadmin -padmin123456 47.xxx.xxx.137:12300
```


#### Step_8--B服务器推送镜像到Harbor仓库 (A服务器)

##### 8.1--更改配置信息 (A服务器)

```bash
# cd /home/apps/harbor
# Global proxy, 对此段信息进行更改.
vim harbor.yml

# 示例
# 172.19.89.106 (A服务器的内网地址)
proxy:
  http_proxy:
  https_proxy:
  no_proxy: "172.19.89.106"
  components:
    - core
    - jobservice
    - trivy
```



##### 8.2--更改配置信息 (B服务器)



`/etc/docker/daemon.json` 配置私有仓库  `insecure-registries` 这里配置 



```bash
vim /etc/docker/daemon.json

# 8.2.1--添加内容-registry-mirrors
"http://172.19.89.106:12300"

# 8.2.2--添加内容-insecure-registries
"http://172.19.89.106:12300"

# 8.2.3--示例
{
 "registry-mirrors": [
        "http://172.19.89.106:12300",
        "https://dockerproxy.com",
        "https://docker.m.daocloud.io",
        "https://dh-mirror.gitverse.ru",
        "https://jockerhub.com",
        "https://njrds9qc.mirror.aliyuncs.com"
  ],
  "insecure-registries": [
       "172.19.89.106:12300"
  ]
}

# 8.2.4--重启docker
systemctl restart docker

# 8.2.5--推送镜像 (B服务器-->A服务器)
docker login -uadmin -padmin123456 172.19.89.106:12300
docker push 172.19.89.106:12300/test-saas-base/test-hello-world:v1
```


#### Step_9--访问Harbor

##### 9.1--访问地址 (IP:Port)

http://47.xxx.xxx.137:12300

##### 9.2--用户名/密码

user--admin  password--admin123456


#### Step_10--上传镜像到Harbor镜像库

```bash
# 10.1--查看所有镜像
docker images

# 登录 harbor 
docker login -uadmin -padmin123456  172.19.89.106:12300

# 10.2--需上传的镜像, 打Tag (加上项目名称 !!!)
#格式--docker tag 镜像名:版本 your-ip:端口/项目名称/新的镜像名:版本
#示例--

docker tag busybox:latest  47.xxx.xxx.137:12300/library/busybox:v1

# 10.3--查看打Tag后的镜像
docker images

# 10.4--上传镜像
#格式--docker push 打Tag后的镜像名
# 推送镜像
docker push 47.xxx.xxx.137:12300/library/busybox:v1
```



#### Step_11--从Harbor镜像库拉取镜像

```bash
# 11.1--拉取方法一
#格式--docker pull 上传时打Tag后的镜像名
docker pull 47.xxx.xxx.137:12300/library/busybox:v1
```



#### 总结 

本文搭建一个Harbor 私有仓库， 方便我们后续存放自己的项目的镜像仓库。 





#### 参考文档 

[harbor官方网址](https://goharbor.io)  

[harbor官方安装教程](https://goharbor.io/docs/2.12.0/install-config/download-installer/)  

[harbor Github地址](https://github.com/goharbor/harbor)  

[参考文档-1](https://juejin.cn/post/7223027325037789241) 

[参考文档-2](https://xie.infoq.cn/article/faa9ee456452891828cc080b8)  

[参考文档-3](https://help.aliyun.com/zh/ecs/use-cases/install-and-use-docker-on-a-linux-ecs-instance#84bc77f47dony) 





<center>  
    <font color=gray size=1  face="黑体">
       分享快乐,留住感动.  '2025-03-15 22:48:56' --frank 
    </font>
</center>





