

## Jenkins 查看安装版本信息

有的时候 需要查看 Jenkins 版本 



### 方法1 登录到服务器上面 

第一种方法 直接登录到服务器 上面 

```bash
[root@xxxxxxxxxxx ~]# jenkins --version  
2.487
```





### 方法2  使用页面上直接查看  

[Dashboard](http://jenkins.zhiexa.com:8900/) >  [系统管理](http://jenkins.zhiexa.com:8900/manage/) > [关于Jenkins](http://jenkins.zhiexa.com:8900/manage/about/) 

```
Jenkins
Version 2.487

Get involved
Jenkins是一个基于社区开发的开源持续集成服务器
```



![image-版本信息](./image/Jenkins查看版本/image-20250308073758676-1390684.png)







### 方法3  查看Jenkins 的版本 

使用`curl` 命令 查看版本 

```bash
curl -I http://jenkins.zhiexa.com:8900/api/json

http://jenkins.zhiexa.com:8900/manage/
user: xxx  
password: xxxx

curl -I  -u <user>:<password> http://jenkins.zhiexa.com:8900/api/json

curl -I  -u <user>:<password> http://jenkins.zhiexa.com:8900/systemInfo
```





```bash
curl -I -u admin:xxxxxx http://jenkins.zhiexa.com:8900/api/json  

HTTP/1.1 200 OK
Server: Jetty(12.0.14)
Date: Fri, 07 Mar 2025 13:01:38 GMT
X-Content-Type-Options: nosniff
X-Jenkins: 2.487
X-Jenkins-Session: 94c95cdb
X-Frame-Options: deny
Content-Type: application/json;charset=utf-8
Content-Length: 14886
---

# 上面显示 X-Jenkins: 2.487 就是Jenkins的版本 
```





